# Multiplayer Action-RPG Game

A base project for a multiplayer action RPG. Features:

- Steam networking using the Online Subsystem and listen servers
    - Server list
    - Joining/hosting lobbies


- Gameplay Ability System
    - Ranged abilities with extensible architecture
    - Melee abilities using Anim Notify System
    - Two player classes, Fire and Ice, with unique ability sets