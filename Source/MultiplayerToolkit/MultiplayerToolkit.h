// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


UENUM(BlueprintType)
enum class EMTAbilityInputID: uint8
{
    // 0 None
    None UMETA(DisplayName = "None"),
    // 1 ConfirmTarget
    ConfirmTarget UMETA(DisplayName = "Confirm Target"),
    // 2 CancelTarget
    CancelTarget UMETA(DisplayName = "Cancel Target"),
    // 3 Light Attack
    LightAttack UMETA(DisplayName = "Light Attack"),
    // 4 Heavy Attack
    HeavyAttack UMETA(DisplayName = "Heavy Attack"),
    // 5 Alternate Attack
    AlternateAttack UMETA(DisplayName="Alternate Attack"),
    // 6 Ultimate Attack
    UltimateAttack UMETA(DisplayName="Ultimate Attack"),
    // 7 Melee Light Attack
    MeleeLightAttack UMETA(DisplayName="Melee Light Attack"),
    //8 Melee Heavy Attack
    MeleeHeavyAttack UMETA(DisplayName="Melee Heavy Attack"),
};

UENUM(BlueprintType)
enum class EMTPlayerClass:uint8
{
    // 0 Fire
    Fire UMETA(DisplayName="Fire"),
    // 1 Ice
    Ice UMETA(DisplayName="Ice"),
    // 2 Earth
    Earth UMETA(DisplayName="Earth"),
    // 3 Wind
    Wind UMETA(DisplayName="Wind")
};
