// Copyright 2020 Xuelong Mu


#include "MTBlueprintLibrary.h"

#include "Kismet/KismetMathLibrary.h"

// Calculate spawn location and rotation of projectile
FTransform UMTBlueprintLibrary::CalculateProjectileTransform(FVector SpawnLocation, FVector ForwardVector)
{
    // Spawn projectile slightly ahead of player
    auto Location = SpawnLocation + ForwardVector * 100;

    auto Rotation = UKismetMathLibrary::Conv_VectorToRotator(ForwardVector);
    return FTransform(Rotation, Location);
}

bool UMTBlueprintLibrary::DoesEffectContainerSpecHaveEffects(const FMTGameplayEffectContainerSpec& ContainerSpec)
{
    return ContainerSpec.HasValidEffects();
}

bool UMTBlueprintLibrary::DoesEffectContainerSpecHaveTargets(const FMTGameplayEffectContainerSpec& ContainerSpec)
{
    return ContainerSpec.HasValidTargets();
}

FMTGameplayEffectContainerSpec UMTBlueprintLibrary::AddTargetsToEffectContainerSpec(
    const FMTGameplayEffectContainerSpec& ContainerSpec, const TArray<FHitResult>& HitResults,
    const TArray<AActor*>& TargetActors)
{
    FMTGameplayEffectContainerSpec NewSpec = ContainerSpec;
    NewSpec.AddTargets(HitResults, TargetActors);
    return NewSpec;
}

TArray<FActiveGameplayEffectHandle> UMTBlueprintLibrary::ApplyExternalEffectContainerSpec(
    const FMTGameplayEffectContainerSpec& ContainerSpec)
{
    TArray<FActiveGameplayEffectHandle> ActiveGEHandles;

    // Iterate list of effect specs and apply them to their target data
    for (const FGameplayEffectSpecHandle& GESpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
    {
        for (const TSharedPtr<FGameplayAbilityTargetData> TargetData : ContainerSpec.TargetData.Data)
        {
            ActiveGEHandles.Append(TargetData->ApplyGameplayEffectSpec(*GESpecHandle.Data.Get()));
        }
    }
    return ActiveGEHandles;
}
