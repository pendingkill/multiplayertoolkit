// Copyright 2020 Xuelong Mu


#include "ScoreboardWidget.h"


#include "ScoreItemWidget.h"
#include "Components/ScrollBox.h"

void UScoreboardWidget::RebuildScoreboard(const TMap<int32, FPlayerInfo>& PlayerInfoMap)
{
    ScoreboardScrollBox->ClearChildren();
    // for (auto& [Id, Player] : PlayerInfoMap) // Requires c++17 flag?
    for (auto& Elem : PlayerInfoMap)
    {
        const FPlayerInfo& Player = Elem.Value;
        auto ScoreItem = CreateWidget<UScoreItemWidget>(this, ScoreItemWidgetClass);
        ScoreItem->Setup(Player.PlayerId, Player.PlayerName, Player.Score);
        ScoreboardScrollBox->AddChild(ScoreItem);
    }
}
