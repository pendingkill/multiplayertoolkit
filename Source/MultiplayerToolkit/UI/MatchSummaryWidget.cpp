// Copyright 2020 Xuelong Mu


#include "MatchSummaryWidget.h"


#include "ScoreboardWidget.h"
#include "Components/TextBlock.h"
#include "Game/PlayerData.h"

void UMatchSummaryWidget::Setup(const FPlayerInfo& WinningPlayerInfo)
{
    FString PlayerName = WinningPlayerInfo.PlayerName;
    MatchResultTextBlock->SetText(FText::Format(
        NSLOCTEXT("ExampleNamespace", "MatchWinningAnnouncement", "{0} has won the match!"),
        FText::FromString(PlayerName)));
}
