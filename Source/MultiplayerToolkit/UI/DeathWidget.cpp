// Copyright 2020 Xuelong Mu


#include "DeathWidget.h"

void UDeathWidget::Setup()
{
    this->bIsFocusable = true;

    auto DeathScreenInputMode = FInputModeUIOnly();
    DeathScreenInputMode.SetWidgetToFocus(this->TakeWidget());
    DeathScreenInputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

    APlayerController* PC = GetOwningPlayer();
    if (PC)
    {
        PC->SetInputMode(DeathScreenInputMode);
        PC->bShowMouseCursor = true;
    }
}

void UDeathWidget::Teardown()
{
    this->RemoveFromViewport();

    auto GameInputMode = FInputModeGameOnly();

    APlayerController* PC = GetOwningPlayer();

    if (!PC) return;

    PC->SetInputMode(GameInputMode);
    PC->bShowMouseCursor = false;
}
