// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Game/PlayerData.h"

#include "ScoreboardWidget.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UScoreboardWidget : public UUserWidget
{
    GENERATED_BODY()
public:
    void RebuildScoreboard(const TMap<int32, FPlayerInfo>& PlayerInfoMap);

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<class UScoreItemWidget> ScoreItemWidgetClass;

private:
    UPROPERTY(meta=(BindWidget))
    class UScrollBox* ScoreboardScrollBox;
};
