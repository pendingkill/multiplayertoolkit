// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"

#include "Blueprint/UserWidget.h"
#include "Game/PlayerData.h"

#include "MTHUDWidget.generated.h"


/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMTHUDWidget : public UUserWidget
{
    GENERATED_BODY()
public:
    /**
    * Attribute setters
    */
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void SetCurrentHealth(float Health);

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void SetCurrentMana(float Mana);

    UFUNCTION()
    void SetCurrentLeader(const FPlayerInfo& PlayerInfo);

    // Implement kill log later
    // UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    // void UpdateKillLog(float Health);

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<class UScoreItemWidget> ScoreWidgetClass;

private:

    UPROPERTY(meta=(BindWidget))
    class UTextBlock* LeaderPlayerTextBlock;
};
