// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MatchSummaryWidget.generated.h"

struct FPlayerInfo;
/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMatchSummaryWidget : public UUserWidget
{
    GENERATED_BODY()
public:

    void Setup(const FPlayerInfo& WinningPlayerInfo);

private:

    UPROPERTY(meta=(BindWidget))
    class UTextBlock* MatchResultTextBlock;
};
