// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"

#include "MultiplayerToolkit.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"

#include "ClassSelectionWidget.generated.h"


/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UClassSelectionWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    UFUNCTION()
    void Setup();

    UFUNCTION()
    void Teardown();

protected:
    virtual bool Initialize() override;

    UFUNCTION()
    void OnClickedFire();

    UFUNCTION()
    void OnClickedIce();

    void ClassSelected(EMTPlayerClass Class);


private:
    UPROPERTY(meta=(BindWidget))
    UButton* FireButton;

    UPROPERTY(meta=(BindWidget))
    UButton* IceButton;

    UPROPERTY(meta=(BindWidget))
    UButton* CancelButton;
};
