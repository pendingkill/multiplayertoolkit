// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DeathWidget.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UDeathWidget : public UUserWidget
{
    GENERATED_BODY()

public:
    UFUNCTION()
    void Setup();

    UFUNCTION()
    void Teardown();
};
