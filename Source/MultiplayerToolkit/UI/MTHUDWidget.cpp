// Copyright 2020 Xuelong Mu


#include "MTHUDWidget.h"


#include "Components/TextBlock.h"

void UMTHUDWidget::SetCurrentLeader(const FPlayerInfo& PlayerInfo)
{
    LeaderPlayerTextBlock->SetText(FText::Format(
        NSLOCTEXT("ExampleNamespace", "LeaderPlayerText", "{0}, {1}"),
        FText::FromString(PlayerInfo.PlayerName), PlayerInfo.Score));
}
