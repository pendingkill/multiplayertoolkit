// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ScoreItemWidget.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UScoreItemWidget : public UUserWidget
{
    GENERATED_BODY()
public:
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
    void Setup(int MyPlayerId, const FString& MyPlayerName, float Score);

    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void UpdateScore(float Score);

    UPROPERTY(BlueprintReadOnly)
    int32 PlayerId;

    UPROPERTY(BlueprintReadOnly)
    FString PlayerName;
};
