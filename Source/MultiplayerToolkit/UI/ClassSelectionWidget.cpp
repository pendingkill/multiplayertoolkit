// Copyright 2020 Xuelong Mu


#include "ClassSelectionWidget.h"

#include "Player/PlayerControllerBase.h"

bool UClassSelectionWidget::Initialize()
{
    if (!Super::Initialize()) return false;
    if (!FireButton || !IceButton || !CancelButton) return false;

    FireButton->OnClicked.AddDynamic(this, &UClassSelectionWidget::OnClickedFire);
    IceButton->OnClicked.AddDynamic(this, &UClassSelectionWidget::OnClickedIce);
    CancelButton->OnClicked.AddDynamic(this, &UClassSelectionWidget::Teardown);

    return true;
}

void UClassSelectionWidget::Setup()
{
    bIsFocusable = true;
    this->AddToViewport();
    
    auto MenuInputMode = FInputModeUIOnly();
    MenuInputMode.SetWidgetToFocus(this->TakeWidget());
    MenuInputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController) return;

    PlayerController->SetInputMode(MenuInputMode);
    PlayerController->bShowMouseCursor = true;
}

void UClassSelectionWidget::Teardown()
{
    this->RemoveFromViewport();
    auto GameInputMode = FInputModeGameOnly();

    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController) return;

    PlayerController->SetInputMode(GameInputMode);
    PlayerController->bShowMouseCursor = false;
}


void UClassSelectionWidget::OnClickedFire()
{
    ClassSelected(EMTPlayerClass::Fire);
}

void UClassSelectionWidget::OnClickedIce()
{
    ClassSelected(EMTPlayerClass::Ice);
}

void UClassSelectionWidget::ClassSelected(EMTPlayerClass Class)
{
    // Want to set UI settings before SetPlayerClass is called, which may show Death UI and override the UI settings from Teardown()
    Teardown();

    auto PC = GetOwningPlayer();
    if (PC)
    {
        auto PCBase = Cast<APlayerControllerBase>(PC);
        if (PCBase)
        {
            PCBase->SetPlayerClass(Class);
        }
    }
}
