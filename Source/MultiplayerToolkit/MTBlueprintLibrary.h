// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"

#include "GAS/MTAbilityTypes.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MTBlueprintLibrary.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMTBlueprintLibrary : public UBlueprintFunctionLibrary
{
    GENERATED_BODY()

    UFUNCTION(BlueprintCallable, Category="Gameplay")
    static FTransform CalculateProjectileTransform(FVector SpawnLocation, FVector ForwardVector);

    // Checks if spec has any effects
    UFUNCTION(BlueprintPure, Category=Ability)
    static bool DoesEffectContainerSpecHaveEffects(const FMTGameplayEffectContainerSpec& ContainerSpec);

    // Checks if spec has any targets
    UFUNCTION(BlueprintPure, Category=Ability)
    static bool DoesEffectContainerSpecHaveTargets(const FMTGameplayEffectContainerSpec& ContainerSpec);

    // Adds targets to a copy of the passed in effect container spec and returns it
    UFUNCTION(BlueprintCallable, Category=Ability, meta=(AutoCreateRefTerm="HitResults,TargetActors"))
    static FMTGameplayEffectContainerSpec AddTargetsToEffectContainerSpec(
        const FMTGameplayEffectContainerSpec& ContainerSpec, const TArray<FHitResult>& HitResults,
        const TArray<AActor*>& TargetActors);

    /** Applies container spec that was made from an ability */
    UFUNCTION(BlueprintCallable, Category = Ability)
    static TArray<FActiveGameplayEffectHandle> ApplyExternalEffectContainerSpec(
        const FMTGameplayEffectContainerSpec& ContainerSpec);
};
