// Copyright 2020 Xuelong Mu


#include "DelayOneFrame.h"

UDelayOneFrame* UDelayOneFrame::WaitForOneFrame(UObject* WorldContextObject, const float MyFloatInput)
{
    UDelayOneFrame* BlueprintNode = NewObject<UDelayOneFrame>();
    BlueprintNode->WorldContextObject = WorldContextObject;
    BlueprintNode->FloatInput = MyFloatInput;
    return BlueprintNode;
}

void UDelayOneFrame::Activate()
{
    WorldContextObject->GetWorld()->GetTimerManager().SetTimerForNextTick(this, &UDelayOneFrame::ExecuteAfterOneFrame);
}

void UDelayOneFrame::ExecuteAfterOneFrame()
{
    AfterOneFrame.Broadcast();
}
