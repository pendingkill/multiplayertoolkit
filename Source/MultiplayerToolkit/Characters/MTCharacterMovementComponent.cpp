// Copyright 2020 Xuelong Mu


#include "MTCharacterMovementComponent.h"


#include "MTCharacterBase.h"
#include "GAS/MTAttributeSetBase.h"

float UMTCharacterMovementComponent::GetMaxSpeed() const
{
    AMTCharacterBase* Owner = Cast<AMTCharacterBase>(GetOwner());
    auto MovementSpeedAttribute = UMTAttributeSetBase::GetMovementSpeedAttribute();
    if (Owner->GetAbilitySystemComponent())
    {
        if (Owner->GetAbilitySystemComponent()->HasAttributeSetForAttribute(MovementSpeedAttribute))
        {
            float MovementSpeedPercentage = Owner->GetAbilitySystemComponent()->GetNumericAttribute(
                MovementSpeedAttribute);
            return Super::GetMaxSpeed() * MovementSpeedPercentage / 100;
        }
    }


    return Super::GetMaxSpeed();
}
