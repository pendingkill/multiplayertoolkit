// Fill out your copyright notice in the Description page of Project Settings.


#include "MTCharacterBase.h"


#include "MTCharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MultiplayerToolkit/GAS/MTAbilitySystemComponent.h"
#include "MultiplayerToolkit/GAS/MTGameplayAbility.h"

// Sets default values
AMTCharacterBase::AMTCharacterBase(const class FObjectInitializer& ObjectInitializer) : Super(
    ObjectInitializer.SetDefaultSubobjectClass<UMTCharacterMovementComponent>(
        ACharacter::CharacterMovementComponentName))
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    bAlwaysRelevant = true;

    EffectRemoveOnDeathTag = FGameplayTag::RequestGameplayTag(FName("Effect.RemoveOnDeath"));
    DeadTag = FGameplayTag::RequestGameplayTag(FName("State.Dead"));
    FallingTag = FGameplayTag::RequestGameplayTag(FName("State.Falling"));

    // MovementModeChangedDelegate.Add(this, )
}

// Called when the game starts or when spawned
void AMTCharacterBase::BeginPlay()
{
    Super::BeginPlay();
}

void AMTCharacterBase::AddCharacterAbilities()
{
    if (GetLocalRole() != ROLE_Authority || !ensure(AbilitySystemComponent) || AbilitySystemComponent->
        CharacterAbilitiesGiven)
    {
        return;
    }

    // TODO: default ability level is 1
    for (auto StartupAbility : CharacterAbilities)
    {
        auto ID = static_cast<int32>(StartupAbility.GetDefaultObject()->AbilityInputID);

        AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility, 1,
                                                                 static_cast<int32>(StartupAbility.GetDefaultObject()->
                                                                     AbilityInputID), this));
    }
    AbilitySystemComponent->CharacterAbilitiesGiven = true;
}

void AMTCharacterBase::RemoveCharacterAbilities()
{
    if (GetLocalRole() != ROLE_Authority || !ensure(AbilitySystemComponent))
    {
        return;
    }
    // Testing whether this built-in method works
    AbilitySystemComponent->ClearAllAbilities();
    AbilitySystemComponent->CharacterAbilitiesGiven = false;
}

void AMTCharacterBase::InitializeAttributes()
{
    if (!ensure(AbilitySystemComponent) || !ensure(DefaultAttributes))
    {
        return;
    }

    FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
    EffectContext.AddSourceObject(this);

    // TODO: default character level is 1
    FGameplayEffectSpecHandle NewEffectSpecHandle = AbilitySystemComponent->MakeOutgoingSpec(
        DefaultAttributes, 1, EffectContext);

    if (NewEffectSpecHandle.IsValid())
    {
        FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(
            *NewEffectSpecHandle.Data.Get(), AbilitySystemComponent);
    }
}

void AMTCharacterBase::AddStartupEffects()
{
    if (GetLocalRole() != ROLE_Authority || !ensure(AbilitySystemComponent) || AbilitySystemComponent->
        StartupEffectsApplied)
    {
        return;
    }

    FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
    EffectContext.AddSourceObject(this);

    for (TSubclassOf<UGameplayEffect> GameplayEffect : StartupEffects)
    {
        FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(
            GameplayEffect, 1, EffectContext);
        if (NewHandle.IsValid())
        {
            FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToTarget(
                *NewHandle.Data.Get(), AbilitySystemComponent);
        }
    }

    AbilitySystemComponent->StartupEffectsApplied = true;
}

void AMTCharacterBase::OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode)
{
    if (!ensure(AbilitySystemComponent))
    {
        return;
    }
    if (PrevMovementMode == EMovementMode::MOVE_Falling)
    {
        // Unsure if falling tag may happen to stack
        AbilitySystemComponent->SetLooseGameplayTagCount(FallingTag, 0);
    }
    Super::OnMovementModeChanged(PrevMovementMode, PreviousCustomMode);
}

void AMTCharacterBase::Falling()
{
    if (!ensure(AbilitySystemComponent))
    {
        return;
    }

    AbilitySystemComponent->AddLooseGameplayTag(FallingTag); // hopefully doesn't accumulate more than 1 time
}


// Called every frame
void AMTCharacterBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

UAbilitySystemComponent* AMTCharacterBase::GetAbilitySystemComponent() const
{
    return AbilitySystemComponent;
}

void AMTCharacterBase::Die()
{
    // Runs on server only
    RemoveCharacterAbilities();
    GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    GetCharacterMovement()->GravityScale = 0;
    GetCharacterMovement()->Velocity = FVector::ZeroVector;

    if (ensure(AbilitySystemComponent))
    {
        AbilitySystemComponent->CancelAllAbilities();
        FGameplayTagContainer EffectTagsToRemove;

        EffectTagsToRemove.AddTag(EffectRemoveOnDeathTag);
        int32 NumEffectsRemoved = AbilitySystemComponent->RemoveActiveEffectsWithTags(EffectTagsToRemove);
        UE_LOG(LogTemp, Warning, TEXT("%d effects removed on death"), NumEffectsRemoved);
        AbilitySystemComponent->AddLooseGameplayTag(DeadTag);
    }

    FinishDying();
}

void AMTCharacterBase::FinishDying()
{
    Destroy();
}
