// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "GameFramework/Character.h"
#include "MTCharacterBase.generated.h"

UCLASS()
class MULTIPLAYERTOOLKIT_API AMTCharacterBase : public ACharacter, public IAbilitySystemInterface
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    AMTCharacterBase(const class FObjectInitializer& ObjectInitializer);

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Implement IAbilitySystemInterface
    virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

    virtual void Die();

    UFUNCTION(BlueprintCallable, Category="MultiplayerToolkit|Character")
    virtual void FinishDying();


protected:
    FGameplayTag DeadTag;
    FGameplayTag EffectRemoveOnDeathTag;
    FGameplayTag FallingTag;

    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "MultiplayerToolkit|Abilities")
    class UMTAbilitySystemComponent* AbilitySystemComponent;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "MultiplayerToolkit|Abilities")
    class UMTAttributeSetBase* AttributeSetBase;

    // Default abilities for this Character. These will be removed on Character death and regiven if Character respawns.
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "MultiplayerToolkit|Abilities")
    TArray<TSubclassOf<class UMTGameplayAbility>> CharacterAbilities;

    // Default attributes for a character for initializing on spawn/respawn.
    // This is an instant GE that overrides the values for attributes that get reset on spawn/respawn.
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "MultiplayerToolkit|Abilities")
    TSubclassOf<class UGameplayEffect> DefaultAttributes;

    // These effects are only applied one time on startup. 
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "MultiplayerToolkit|Abilities")
    TArray<TSubclassOf<class UGameplayEffect>> StartupEffects;

    // Grant abilities on the Server. The Ability Specs will be replicated to the owning client.
    virtual void AddCharacterAbilities();

    // Removes all CharacterAbilities. Can only be called by the Server. Removing on the Server will remove from Client too.
    virtual void RemoveCharacterAbilities();

    // Initialize the Character's attributes. Must run on Server but we run it on Client too
    // so that we don't have to wait. The Server's replication to the Client won't matter since
    // the values should be the same.
    virtual void InitializeAttributes();

    virtual void AddStartupEffects();

    virtual void OnMovementModeChanged(EMovementMode PrevMovementMode, uint8 PreviousCustomMode) override;
    virtual void Falling() override;
};
