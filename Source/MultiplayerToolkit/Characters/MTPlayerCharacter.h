// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MTCharacterBase.h"
#include "GameFramework/Character.h"
#include "MTPlayerCharacter.generated.h"

UCLASS()
class MULTIPLAYERTOOLKIT_API AMTPlayerCharacter : public AMTCharacterBase
{
    GENERATED_BODY()

public:
    // Sets default values for this character's properties
    AMTPlayerCharacter(const class FObjectInitializer& ObjectInitializer);

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    // Only called on the Server. Calls before Server's AcknowledgePossession.
    virtual void PossessedBy(AController* NewController) override;


    UPROPERTY(Category="Character", BlueprintReadWrite, VisibleAnywhere)
    class USpringArmComponent* CameraBoom;

    UPROPERTY(Category="Character",BlueprintReadWrite, VisibleAnywhere)
    class UCameraComponent* FollowCamera;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
    class UStaticMeshComponent* Weapon;

    virtual void FinishDying() override;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    // Client only
    virtual void OnRep_PlayerState() override;

    // Called from both SetupPlayerInputComponent and OnRep_PlayerState because of a potential race condition where the PlayerController might
    // call ClientRestart which calls SetupPlayerInputComponent before the PlayerState is repped to the client so the PlayerState would be null in SetupPlayerInputComponent.
    // Conversely, the PlayerState might be repped before the PlayerController calls ClientRestart so the Actor's InputComponent would be null in OnRep_PlayerState.
    void BindASCInput();

    bool ASCInputBound = false;

    UPROPERTY(BlueprintReadOnly, Category="Camera")
    FVector StartingCameraBoomLocation;
};
