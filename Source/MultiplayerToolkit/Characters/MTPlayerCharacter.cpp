// Fill out your copyright notice in the Description page of Project Settings.


#include "MTPlayerCharacter.h"


#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "MultiplayerToolkit/Game/MTGameMode.h"
#include "MultiplayerToolkit/MultiplayerToolkit.h"
#include "MultiplayerToolkit/Game/MTGameState.h"
#include "MultiplayerToolkit/GAS/MTAbilitySystemComponent.h"
#include "MultiplayerToolkit/Player/MTPlayerController.h"
#include "MultiplayerToolkit/Player/MTPlayerState.h"

// Sets default values
AMTPlayerCharacter::AMTPlayerCharacter(const class FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
    // Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(FName("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->bUsePawnControlRotation = true;
    CameraBoom->SetRelativeLocation(FVector(0, 0, 8.5));

    FollowCamera = CreateDefaultSubobject<UCameraComponent>(FName("FollowCamera"));
    FollowCamera->SetupAttachment(CameraBoom);
    FollowCamera->FieldOfView = 80.0f;

    Weapon = CreateDefaultSubobject<UStaticMeshComponent>(FName("Weapon"));
    Weapon->SetupAttachment(GetMesh(), "melee_Socket");

    // Makes sure that the animations play on the Server so that we can use bone and socket transforms
    // to do things like spawning projectiles and other FX.
    GetMesh()->VisibilityBasedAnimTickOption = EVisibilityBasedAnimTickOption::AlwaysTickPoseAndRefreshBones;
    GetMesh()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    GetMesh()->SetCollisionProfileName(FName("NoCollision"));
}


// Called when the game starts or when spawned
void AMTPlayerCharacter::BeginPlay()
{
    Super::BeginPlay();
    StartingCameraBoomLocation = CameraBoom->GetRelativeLocation();
}


// Called every frame
void AMTPlayerCharacter::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMTPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);
    BindASCInput();
}

// Server only. Many functions are repeated at OnRep_PlayerState for clients
void AMTPlayerCharacter::PossessedBy(AController* NewController)
{
    Super::PossessedBy(NewController);

    AMTPlayerState* PS = GetPlayerState<AMTPlayerState>();
    if (!ensure(PS))
    {
        return;
    }

    // Set the ASC on the Server. Clients do this in OnRep_PlayerState()
    AbilitySystemComponent = Cast<UMTAbilitySystemComponent>(PS->GetAbilitySystemComponent());

    // AI won't have PlayerControllers so we can init again here just to be sure. No harm in initing twice for heroes that have PlayerControllers.
    PS->GetAbilitySystemComponent()->InitAbilityActorInfo(PS, this);

    // Set the AttributeSetBase for convenience attribute functions
    AttributeSetBase = PS->GetAttributeSetBase();

    // Handle differently if players disconnect/reconnect in future. For now assume possession = spawn/respawn.
    InitializeAttributes();

    AddStartupEffects();

    AddCharacterAbilities();


    AMTPlayerController* PC = Cast<AMTPlayerController>(GetController());
    if (PC)
    {
        PC->CreateHUD();
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Server side HUD set up failed"));
    }
}

// Client only
void AMTPlayerCharacter::OnRep_PlayerState()
{
    APlayerState* PSBase = GetPlayerState<APlayerState>();
    if (!PSBase)
    {
        UE_LOG(LogTemp, Error, TEXT("PlayerStateBase not found in OnRep_PlayerState"));
    }

    AMTPlayerState* PS = GetPlayerState<AMTPlayerState>();

    if (!PS)
    {
        UE_LOG(LogTemp, Error, TEXT("AMTPlayerState not found in OnRep_PlayerState"));

        return;
    }

    // Set the ASC for clients. Server does this in PossessedBy.
    AbilitySystemComponent = Cast<UMTAbilitySystemComponent>(PS->GetAbilitySystemComponent());

    // Init ASC Actor Info for clients. Server will init its ASC when it possesses a new Actor.
    AbilitySystemComponent->InitAbilityActorInfo(PS, this);

    // Bind player input to the AbilitySystemComponent. Also called in SetupPlayerInputComponent because of a potential
    // race condition (relevant for clients only)
    BindASCInput();

    // Just enabled this 9/26
    AttributeSetBase = PS->GetAttributeSetBase();

    InitializeAttributes();

    AMTPlayerController* PC = Cast<AMTPlayerController>(GetController());
    if (PC)
    {
        // UE_LOG(LogTemp, Warning, TEXT("PC found in client side HUD set up"));
        PC->CreateHUD();
    }
}


void AMTPlayerCharacter::BindASCInput()
{
    if (!ASCInputBound && AbilitySystemComponent && IsValid(InputComponent))
    {
        AbilitySystemComponent->
            BindAbilityActivationToInputComponent(InputComponent, FGameplayAbilityInputBinds(
                                                      FString("ConfirmTarget"),
                                                      FString("CancelTarget"),
                                                      FString("EMTAbilityInputID"),
                                                      static_cast<int32>(EMTAbilityInputID::ConfirmTarget),
                                                      static_cast<int32>(EMTAbilityInputID::CancelTarget)));
        ASCInputBound = true;

        return;
    }
}

void AMTPlayerCharacter::FinishDying()
{
    auto PC = Cast<AMTPlayerController>(GetController());
    if (ensure(PC))
    {
        PC->ShowDeathUI();
    }

    if (GetLocalRole() == ROLE_Authority)
    {
        auto GM = Cast<AMTGameMode>(GetWorld()->GetAuthGameMode());
        if (ensure(GM))
        {
            // Controller will dispossess here
            GM->HeroDied(GetController());
        }
    }

    Super::FinishDying();
}
