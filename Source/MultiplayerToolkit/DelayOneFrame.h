// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "DelayOneFrame.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDelayOneFrameOutputPin);

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UDelayOneFrame : public UBlueprintAsyncActionBase
{
    GENERATED_BODY()
public:
    UPROPERTY(BlueprintAssignable)
    FDelayOneFrameOutputPin AfterOneFrame;

    UFUNCTION(BlueprintCallable, meta = (BlueprintInternaluseONly = "true", WorldContext = "WorldContextObject"),
        Category = "Flow Control")
    static UDelayOneFrame* WaitForOneFrame(UObject* WorldContextObject, const float MyFloatInput);


    // UBlueprintAsyncActionBase interface
    virtual void Activate() override;
    //~UBlueprintAsyncActionBase interface

private:
    UFUNCTION()
    void ExecuteAfterOneFrame();

    UObject* WorldContextObject = nullptr;
    float FloatInput = 0.0f;
};
