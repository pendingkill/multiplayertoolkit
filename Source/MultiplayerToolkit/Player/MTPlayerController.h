// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "PlayerControllerBase.h"
#include "MultiplayerToolkit/UI/MTHUDWidget.h"
#include "MultiplayerToolkit/Game/PlayerData.h"

#include "MTPlayerController.generated.h"

struct FPlayerInfo;

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API AMTPlayerController : public APlayerControllerBase
{
    GENERATED_BODY()
public:
    void CreateHUD();
    void CheckGameStateScoreboard();
    virtual void PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel) override;

    UFUNCTION(Client, Reliable)
    void ShowDeathUI();
    void ShowDeathUI_Implementation();

    UFUNCTION(Client, Reliable)
    void ShowHUD();
    void ShowHUD_Implementation();

    UFUNCTION(Client, Reliable)
    void RebuildScoreboard(const TArray<FPlayerInfo>& PlayerInfo);
    void RebuildScoreboard_Implementation(const TArray<FPlayerInfo>& PlayerInfo);

    void UpdateScore(int32 PlayerId, float Score);
    void SortLocalScoreMap();

    UFUNCTION(Client, Reliable)
    void HandleMatchHasEnded();
    void HandleMatchHasEnded_Implementation();

    virtual void SetupInputComponent() override;

protected:
    virtual void OnRep_PlayerState() override;

    //HUD widget here
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MultiplayerToolkit|UI")
    TSubclassOf<class UMTHUDWidget> UIHUDWidgetClass;

    UPROPERTY(BlueprintReadWrite, Category = "MultiplayerToolkit|UI")
    class UMTHUDWidget* UIHUDWidget;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="MultiplayerToolkit|UI")
    TSubclassOf<class UScoreboardWidget> ScoreboardWidgetClass;

    UPROPERTY(BlueprintReadWrite, Category = "MultiplayerToolkit|UI")
    class UScoreboardWidget* ScoreboardWidget;

    UPROPERTY(BlueprintReadWrite, EditAnywhere,Category="MultiplayerToolkit|UI")
    TSubclassOf<class UMatchSummaryWidget> MatchSummaryWidgetClass;

    UPROPERTY(BlueprintReadWrite, Category="MultiplayerToolkit|UI")
    class UMatchSummaryWidget* MatchSummaryWidget;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="MultiplayerToolkit|UI")
    TSubclassOf<class UDeathWidget> DeathWidgetClass;

    UPROPERTY(BlueprintReadWrite, Category = "MultiplayerToolkit|UI")
    class UDeathWidget* DeathWidget;

    UPROPERTY(BlueprintReadOnly)
    TMap<int32, FPlayerInfo> LocalPlayerInfoMap;

    // Server Only
    virtual void OnPossess(APawn* InPawn) override;

private:
    void ToggleScoreboard();
    bool MatchEnded;
    FTimerHandle GameStateReplicationCheckTimer;
};
