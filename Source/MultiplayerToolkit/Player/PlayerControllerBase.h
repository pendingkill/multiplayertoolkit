// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"

#include "MultiplayerToolkit.h"
#include "GameFramework/PlayerController.h"
#include "PlayerControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API APlayerControllerBase : public APlayerController
{
    GENERATED_BODY()
public:
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Replicated)
    EMTPlayerClass PlayerClass = EMTPlayerClass::Fire;

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
    bool DieOnClassChange;

    UFUNCTION(Server, Reliable, BlueprintCallable)
    void SetPlayerClass(EMTPlayerClass ChosenClass);
    void SetPlayerClass_Implementation(EMTPlayerClass ChosenClass);

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    APlayerControllerBase();

    virtual void SetupInputComponent() override;

    UFUNCTION(BlueprintCallable)
    void ShowClassSelectionMenu();

    void ToggleEscapeMenu();

protected:
    UPROPERTY()
    class UClassSelectionWidget* ClassSelectionMenu;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MultiplayerToolkit|UI")
    TSubclassOf<class UEscapeMenu> EscapeMenuClass;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "MultiplayerToolkit|UI")
    TSubclassOf<class UClassSelectionWidget> ClassSelectionMenuClass;

private:
    UPROPERTY()
    class UEscapeMenu* EscapeMenu;
};
