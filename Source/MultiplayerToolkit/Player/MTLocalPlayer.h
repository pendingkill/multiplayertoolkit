// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Engine/LocalPlayer.h"
#include "MTLocalPlayer.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMTLocalPlayer : public ULocalPlayer
{
    GENERATED_BODY()
public:
    virtual FString GetNickname() const override;
};
