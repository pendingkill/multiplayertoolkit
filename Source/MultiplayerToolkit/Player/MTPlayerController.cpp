// Fill out your copyright notice in the Description page of Project Settings.


#include "MTPlayerController.h"


#include "AbilitySystemComponent.h"
#include "MTPlayerState.h"
#include "Blueprint/UserWidget.h"
#include "GameFramework/GameStateBase.h"
#include "MultiplayerToolkit/Game/MTGameState.h"
#include "MultiplayerToolkit/UI/MTHUDWidget.h"
#include "UI/ClassSelectionWidget.h"
#include "UI/DeathWidget.h"
#include "UI/MatchSummaryWidget.h"
#include "UI/ScoreboardWidget.h"


const static float GAME_STATE_CHECK_DELAY = 1.0f;

void AMTPlayerController::OnRep_PlayerState()
{
    Super::OnRep_PlayerState();
    // For edge cases where a) the PlayerState has not been repped when the Hero is Possessed, or
    // b) PlayerState is repped before the Hero is possessed
    CreateHUD();
}

void AMTPlayerController::CreateHUD()
{
    UE_LOG(LogTemp, Warning, TEXT("Create HUD called"));

    if (UIHUDWidget)
    {
        return;
    }
    if (!UIHUDWidgetClass)
    {
        UE_LOG(LogTemp, Error, TEXT("UI HUD Widget class not set in PlayerController"));
        return;
    }
    if (!IsLocalPlayerController())
    {
        UE_LOG(LogTemp, Warning, TEXT("CreateHUD exiting, called on non-Local Player controller"));
        return;
    }

    if (DeathWidget)
    {
        DeathWidget->Teardown();
        DeathWidget = nullptr;
    }

    auto PS = GetPlayerState<AMTPlayerState>();
    if (!PS)
    {
        UE_LOG(LogTemp, Warning, TEXT("CreateHUD exiting, PS not found yet"));
        return;
    }

    UIHUDWidget = CreateWidget<UMTHUDWidget>(this, UIHUDWidgetClass);
    UIHUDWidget->AddToViewport();

    UIHUDWidget->SetCurrentHealth(PS->GetHealth());
    UIHUDWidget->SetCurrentMana(PS->GetMana());

    // Added initial delay to let PlayerArray finish populating in the GameState
    GetWorldTimerManager().SetTimer(GameStateReplicationCheckTimer, this,
                                    &AMTPlayerController::CheckGameStateScoreboard, GAME_STATE_CHECK_DELAY, true,
                                    GAME_STATE_CHECK_DELAY);

    if (!ScoreboardWidgetClass)
    {
        UE_LOG(LogTemp, Error, TEXT("Scoreboard Widget class not set in PlayerController"));
        return;
    }
    ScoreboardWidget = CreateWidget<UScoreboardWidget>(this, ScoreboardWidgetClass);
    ScoreboardWidget->SetVisibility(ESlateVisibility::Collapsed);
    ScoreboardWidget->AddToViewport();
}

void AMTPlayerController::CheckGameStateScoreboard()
{
    auto GS = GetWorld()->GetGameState();
    if (GS)
    {
        auto MTGS = Cast<AMTGameState>(GS);
        if (MTGS)
        {
            GetWorldTimerManager().ClearTimer(GameStateReplicationCheckTimer);
            UE_LOG(LogTemp, Warning, TEXT("RetrievePlayerScoreboardInfo called for %s"),
                   *GetPlayerState<APlayerState>()->GetPlayerName());
            MTGS->RequestPlayerScoreboardInfo(this);
        }
        else
        {
            UE_LOG(LogTemp, Warning, TEXT("Cast to MTGameState not successful"));
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("GameStateBase not found"));
    }
}

/** Remove HUD elements and clear references */
void AMTPlayerController::PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel)
{
    if (MatchSummaryWidget)
    {
        MatchSummaryWidget->RemoveFromViewport();
    }
    if (ScoreboardWidget)
    {
        ScoreboardWidget->RemoveFromViewport();
    }
    if (UIHUDWidget)
    {
        UIHUDWidget->RemoveFromViewport();
    }
    MatchSummaryWidget = nullptr;
    ScoreboardWidget = nullptr;
    UIHUDWidget = nullptr;
    MatchEnded = false;

    Super::PreClientTravel(PendingURL, TravelType, bIsSeamlessTravel);
}


void AMTPlayerController::ShowDeathUI_Implementation()
{
    if (!DeathWidgetClass)
    {
        UE_LOG(LogTemp, Error, TEXT("Death screen widget not set in Player Controller"));
        return;
    }

    if (IsLocalPlayerController())
    {
        if (UIHUDWidget)
        {
            UIHUDWidget->SetVisibility(ESlateVisibility::Hidden);
        }

        DeathWidget = CreateWidget<UDeathWidget>(this, DeathWidgetClass);
        if (DeathWidget)
        {
            DeathWidget->Setup();
            DeathWidget->AddToViewport();
        }
    }
}

// When respawning
void AMTPlayerController::ShowHUD_Implementation()
{
    if (!IsLocalPlayerController())
    {
        return;
    }
    if (DeathWidget)
    {
        DeathWidget->Teardown();
    }
    if (ClassSelectionMenu)
    {
        ClassSelectionMenu->Teardown();
    }
    if (UIHUDWidget)
    {
        UIHUDWidget->SetVisibility(ESlateVisibility::Visible);
    }
}

void AMTPlayerController::RebuildScoreboard_Implementation(const TArray<FPlayerInfo>& PlayerInfo)
{
    if (!IsLocalPlayerController())
    {
        return;
    }
    if (!UIHUDWidget)
    {
        UE_LOG(LogTemp, Warning, TEXT("UIHUDWidget not found when rebuilding scoreboard"));
        return;
    }
    for (auto MyPlayer : PlayerInfo)
    {
        LocalPlayerInfoMap.Add(MyPlayer.PlayerId, MyPlayer);
    }
    ScoreboardWidget->RebuildScoreboard(LocalPlayerInfoMap);
}

void AMTPlayerController::UpdateScore(int32 PlayerId, float Score)
{
    if (!IsLocalPlayerController())
    {
        return;
    }
    if (!ensure(ScoreboardWidget))
    {
        UE_LOG(LogTemp, Warning, TEXT("UIHUDWidget not found when updating score"));
        return;
    }

    if (!LocalPlayerInfoMap.Contains(PlayerId))
    {
        UE_LOG(LogTemp, Warning, TEXT("PlayerId %d not found when updating score"), PlayerId);
        return;
    }

    LocalPlayerInfoMap.Find(PlayerId)->Score = Score;
    SortLocalScoreMap();
    ScoreboardWidget->RebuildScoreboard(LocalPlayerInfoMap);

    // Update current leader by getting the very first element in the map, then exit
    for (const TTuple<int, FPlayerInfo>& Elem : LocalPlayerInfoMap)
    {
        UIHUDWidget->SetCurrentLeader(Elem.Value);
        break;
    }
}

void AMTPlayerController::SortLocalScoreMap()
{
    LocalPlayerInfoMap.ValueStableSort([](const FPlayerInfo& A, const FPlayerInfo& B) { return A.Score > B.Score; });
}

/** Handle end of match and display winning player on the HUD. Currently this uses the first element of the local player info map.
 * If this proves to have issues with the incorrect player being displayed as the winner, we can pass in the winning player info
 * directly from the calling GameMode instead.
 */
void AMTPlayerController::HandleMatchHasEnded_Implementation()
{
    if (!IsLocalPlayerController())
    {
        return;
    }
    MatchEnded = true;

    if (DeathWidget)
    {
        DeathWidget->RemoveFromViewport();
        DeathWidget = nullptr; // Potentially need to move this outside of conditional
    }

    if (ScoreboardWidget)
    {
        ScoreboardWidget->SetVisibility(ESlateVisibility::Visible);
    }

    if (MatchSummaryWidgetClass)
    {
        MatchSummaryWidget = CreateWidget<UMatchSummaryWidget>(this, MatchSummaryWidgetClass);
        for (const TTuple<int, FPlayerInfo>& Elem : LocalPlayerInfoMap)
        {
            UE_LOG(LogTemp, Warning, TEXT("Winning Player: %s"), *Elem.Value.PlayerName);
            MatchSummaryWidget->Setup(Elem.Value);
            break;
        }
        MatchSummaryWidget->AddToViewport();
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("MatchSummaryWidgetClass not set in player controller blueprint"));
    }

    // this->UnPossess(); // This causes OnRep_PlayerState to be called and PlayerState is null.

    // Player might be dead, so include a check
    if (GetPawn())
    {
        GetPawn()->DisableInput(this);
    }
}

void AMTPlayerController::SetupInputComponent()
{
    Super::SetupInputComponent();
    InputComponent->BindAction(TEXT("ToggleScoreboard"), EInputEvent::IE_Pressed, this,
                               &AMTPlayerController::ToggleScoreboard);
    InputComponent->BindAction(TEXT("ToggleScoreboard"), EInputEvent::IE_Released, this,
                               &AMTPlayerController::ToggleScoreboard);
}

// ReSharper disable once CppMemberFunctionMayBeConst
void AMTPlayerController::ToggleScoreboard()
{
    if (MatchEnded | !ScoreboardWidget) return;
    ScoreboardWidget->Visibility == ESlateVisibility::Collapsed
        ? ScoreboardWidget->SetVisibility(ESlateVisibility::Visible)
        : ScoreboardWidget->SetVisibility(ESlateVisibility::Collapsed);
}

void AMTPlayerController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    // When respawning
    ShowHUD();

    AMTPlayerState* PS = GetPlayerState<AMTPlayerState>();
    if (ensure(PS))
    {
        PS->GetAbilitySystemComponent()->InitAbilityActorInfo(PS, InPawn);
    }
}
