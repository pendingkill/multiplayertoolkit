// Copyright 2020 Xuelong Mu


#include "PlayerControllerBase.h"

#include "Blueprint/UserWidget.h"
#include "Characters/MTPlayerCharacter.h"
#include "MultiplayerToolkit/Game/MTGameInstance.h"
#include "MultiplayerToolkit/MenuSystem/EscapeMenu.h"
#include "Net/UnrealNetwork.h"
#include "UObject/ConstructorHelpers.h"
#include "MultiplayerToolkit/UI/ClassSelectionWidget.h"

APlayerControllerBase::APlayerControllerBase()
{
    EscapeMenuClass = ConstructorHelpers::FClassFinder<UUserWidget>(TEXT("/Game/MenuSystem/WBP_EscapeMenu")).Class;
    if (!EscapeMenuClass)
    {
        UE_LOG(LogTemp, Error, TEXT("WBP_EscapeMenu not found."))
    }
}

void APlayerControllerBase::SetPlayerClass_Implementation(EMTPlayerClass ChosenClass)
{
    PlayerClass = ChosenClass;
    if (DieOnClassChange)
    {
        auto PlayerCharacter = Cast<AMTPlayerCharacter>(GetPawn());
        if (PlayerCharacter)
        {
            PlayerCharacter->Die();
        }
    }
}

void APlayerControllerBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    DOREPLIFETIME(APlayerControllerBase, PlayerClass);
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
}

void APlayerControllerBase::SetupInputComponent()
{
    Super::SetupInputComponent();
    InputComponent->BindAction(TEXT("EscapeMenu"), EInputEvent::IE_Pressed, this,
                               &APlayerControllerBase::ToggleEscapeMenu);
    InputComponent->BindAction(TEXT("SelectClass"), EInputEvent::IE_Pressed, this,
                               &APlayerControllerBase::ShowClassSelectionMenu);
}

void APlayerControllerBase::ShowClassSelectionMenu()
{
    if (!ClassSelectionMenuClass)
    {
        UE_LOG(LogTemp, Error, TEXT("ClassSelectionMenuClass not defined in PlayerController blueprints."))
        return;
    }
    ClassSelectionMenu = CreateWidget<UClassSelectionWidget>(this, ClassSelectionMenuClass);
    ClassSelectionMenu->Setup();
}

void APlayerControllerBase::ToggleEscapeMenu()
{
    if (GetLevel()->GetName() == "MainMenu") return;

    if (!EscapeMenuClass)
    {
        UE_LOG(LogTemp, Error, TEXT("EscapeMenuClass not defined in PlayerController blueprints."))
        return;
    }

    EscapeMenu = CreateWidget<UEscapeMenu>(GetGameInstance(), EscapeMenuClass);
    EscapeMenu->Setup();
    const auto MTGI = Cast<UMTGameInstance>(GetGameInstance());
    if (!MTGI)
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to cast GameInstance to PuzzlePlatformsGameInstance"));
        return;
    }
    EscapeMenu->SetMenuInterface(MTGI);
}
