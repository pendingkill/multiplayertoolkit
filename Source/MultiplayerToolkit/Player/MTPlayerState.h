// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"
#include "GameplayEffectTypes.h"
#include "MTPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API AMTPlayerState : public APlayerState, public IAbilitySystemInterface
{
    GENERATED_BODY()

public:
    AMTPlayerState();

    virtual class UAbilitySystemComponent* GetAbilitySystemComponent() const override;

    class UMTAttributeSetBase* GetAttributeSetBase() const;

    UFUNCTION(BlueprintCallable, Category = "MultiplayerTemplate|MTPlayerState|Attributes")
    float GetHealth() const;

    UFUNCTION(BlueprintCallable, Category = "MultiplayerTemplate|MTPlayerState|Attributes")
    float GetMana() const;

protected:

    UPROPERTY()
    class UMTAbilitySystemComponent* AbilitySystemComponent;

    UPROPERTY()
    class UMTAttributeSetBase* AttributeSetBase;

    FGameplayTag DeadTag;

    FDelegateHandle HealthChangedDelegateHandle;
    FDelegateHandle ManaChangedDelegateHandle;

    virtual void BeginPlay() override;

    virtual void HealthChanged(const FOnAttributeChangeData& Data);
    virtual void ManaChanged(const FOnAttributeChangeData& Data);

    virtual void OnRep_Score() override;
};
