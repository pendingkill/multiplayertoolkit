// Copyright 2020 Xuelong Mu


#include "MTLocalPlayer.h"

#include "Game/MTGameInstance.h"

FString UMTLocalPlayer::GetNickname() const
{
    auto MTGI = Cast<UMTGameInstance>(GetGameInstance());
    if (MTGI)
    {
        auto Name = MTGI->CustomPlayerName;
        if (!Name.IsEmpty())
        {
            return Name;
        }
    }
    return Super::GetNickname();
}
