// Fill out your copyright notice in the Description page of Project Settings.


#include "MTPlayerState.h"


#include "GameplayEffectExtension.h"
#include "MTPlayerController.h"
#include "Engine/Engine.h"
#include "Game/MTGameState.h"
#include "Kismet/GameplayStatics.h"
#include "MultiplayerToolkit/Characters/MTPlayerCharacter.h"
#include "MultiplayerToolkit/Game/MTGameMode.h"
#include "MultiplayerToolkit/GAS/MTAbilitySystemComponent.h"
#include "MultiplayerToolkit/GAS/MTAttributeSetBase.h"

AMTPlayerState::AMTPlayerState()
{
    AbilitySystemComponent = CreateDefaultSubobject<UMTAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
    AbilitySystemComponent->SetIsReplicated(true);

    // Mixed mode means we only are replicated the GEs to ourself, not the GEs to simulated proxies. If another MTPlayerState (Hero) receives a GE,
    // we won't be told about it by the Server. Attributes, GameplayTags, and GameplayCues will still replicate to us.
    AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Mixed);

    // Create the attribute set, this replicates by default
    // Adding it as a subobject of the owning actor of an AbilitySystemComponent
    // automatically registers the AttributeSet with the AbilitySystemComponent
    AttributeSetBase = CreateDefaultSubobject<UMTAttributeSetBase>(TEXT("AttributeSetBase"));

    // Set PlayerState's NetUpdateFrequency to the same as the Character.
    // Default is very low for PlayerStates and introduces perceived lag in the ability system.
    // 100 is probably way too high for a shipping game, you can adjust to fit your needs.
    NetUpdateFrequency = 100.0f;
}

UAbilitySystemComponent* AMTPlayerState::GetAbilitySystemComponent() const
{
    return AbilitySystemComponent;
}

UMTAttributeSetBase* AMTPlayerState::GetAttributeSetBase() const
{
    return AttributeSetBase;
}

float AMTPlayerState::GetHealth() const
{
    return AttributeSetBase->GetHealth();
}

float AMTPlayerState::GetMana() const
{
    return AttributeSetBase->GetMana();
}

void AMTPlayerState::BeginPlay()
{
    Super::BeginPlay();

    if (ensure(AbilitySystemComponent))
    {
        HealthChangedDelegateHandle = AbilitySystemComponent->
                                      GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetHealthAttribute()).
                                      AddUObject(this, &AMTPlayerState::HealthChanged);
        ManaChangedDelegateHandle = AbilitySystemComponent->
                                    GetGameplayAttributeValueChangeDelegate(AttributeSetBase->GetManaAttribute()).
                                    AddUObject(this, &AMTPlayerState::ManaChanged);
    }
}

void AMTPlayerState::HealthChanged(const FOnAttributeChangeData& Data)
{
    float Health = Data.NewValue;
    // Update player widget  health bar

    if (Health <= 0.0f && !AbilitySystemComponent->HasMatchingGameplayTag(DeadTag))
    {
        auto Player = Cast<AMTPlayerCharacter>(GetPawn());
        if (Player)
        {
            Player->Die();
            auto GM = Cast<AMTGameMode>(GetWorld()->GetAuthGameMode());
            if (GM && GetLocalRole() == ROLE_Authority)
            {
                UE_LOG(LogTemp, Warning, TEXT("Game mode found"));

                auto InstigatorActor = Data.GEModData->EffectSpec.GetEffectContext().GetInstigator();
                if (InstigatorActor)
                {
                    auto PlayerState = Cast<APlayerState>(InstigatorActor);

                    if (PlayerState)
                    {
                        auto InstigatorController = PlayerState->GetPawn()->GetInstigatorController();
                        if (InstigatorController)
                        {
                            UE_LOG(LogTemp, Warning, TEXT("Instigator Controller: %s"),
                                   *InstigatorController->GetName());
                            GM->RegisterPlayerKilled(Cast<AController>(GetOwner()), InstigatorController);
                        }
                        else
                        {
                            UE_LOG(LogTemp, Warning, TEXT("PlayerState found but Instigator controller not found"));
                        }
                    }
                    else
                    {
                        UE_LOG(LogTemp, Error, TEXT("InstigatorActor to PlayerState cast unsuccessful"));
                    }
                }
                else
                {
                    UE_LOG(LogTemp, Error, TEXT("PlayerState not found"));
                }
            }
        }
    }
}

void AMTPlayerState::ManaChanged(const FOnAttributeChangeData& Data)
{
    float Mana = Data.NewValue;
}

void AMTPlayerState::OnRep_Score()
{
    auto PC = UGameplayStatics::GetPlayerController(this, 0);

    // Should not run on dedicated servers
    if (!PC->IsLocalPlayerController())
    {
        return;
    }

    auto MTPC = Cast<AMTPlayerController>(PC);
    if (MTPC)
    {
        MTPC->UpdateScore(GetPlayerId(), GetScore());
    }
}
