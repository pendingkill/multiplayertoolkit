// Copyright 2020 Xuelong Mu


#include "MMC_Damage_Ice_Heavy.h"

UMMC_Damage_Ice_Heavy::UMMC_Damage_Ice_Heavy()
{
}

float UMMC_Damage_Ice_Heavy::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
    float DamageMultiplier = 1.0f;
    const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

    if (TargetTags->HasTagExact(FGameplayTag::RequestGameplayTag(FName("Effect.Chill"))))
    {
        DamageMultiplier *= 2;
    }
    UE_LOG(LogTemp, Warning, TEXT("Damage multiplication factor: %f" ), DamageMultiplier);
    return DamageMultiplier;
}
