// Copyright 2020 Xuelong Mu


#include "MMC_Damage_Fire_SetByCaller.h"
#include "GameplayEffect.h"

float UMMC_Damage_Fire_SetByCaller::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
    float DamageMultiplier = 1.0f;
    const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

    if (TargetTags->HasTagExact(FGameplayTag::RequestGameplayTag(FName("Effect.Heat"))))
    {
        DamageMultiplier *= 1.5;
    }
    UE_LOG(LogTemp, Warning, TEXT("Damage multiplication factor: %f" ), DamageMultiplier);

    float BaseDamage = Spec.GetSetByCallerMagnitude(
        FGameplayTag::RequestGameplayTag(FName("Effect.SplashDamage")));
    // UE_LOG(LogTemp, Warning, TEXT("Base radial damage: %f" ), BaseRadialDamage);

    return BaseDamage * DamageMultiplier;
}
