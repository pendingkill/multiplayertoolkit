// Copyright 2020 Xuelong Mu


#include "MMC_Damage_Fire.h"

UMMC_Damage_Fire::UMMC_Damage_Fire()
{
}

float UMMC_Damage_Fire::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
    float DamageMultiplier = 1.0f;
    const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

    if (TargetTags->HasTagExact(FGameplayTag::RequestGameplayTag(FName("Effect.Heat"))))
    {
        DamageMultiplier *= 1.5;
    }
    UE_LOG(LogTemp, Warning, TEXT("Damage multiplication factor: %f" ), DamageMultiplier);
    return DamageMultiplier;
}
