// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "MMC_Damage_Fire.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMMC_Damage_Fire : public UGameplayModMagnitudeCalculation
{
    GENERATED_BODY()

    UMMC_Damage_Fire();

    virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
};
