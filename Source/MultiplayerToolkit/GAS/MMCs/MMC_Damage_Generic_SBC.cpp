// Copyright 2020 Xuelong Mu


#include "MMC_Damage_Generic_SBC.h"
#include "GameplayEffect.h"

float UMMC_Damage_Generic_SBC::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
    float Damage = Spec.GetSetByCallerMagnitude(
        FGameplayTag::RequestGameplayTag(FName("Effect.SplashDamage")));

    return Damage;
}
