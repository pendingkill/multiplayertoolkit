// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "MMC_Damage_Generic_SBC.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMMC_Damage_Generic_SBC : public UGameplayModMagnitudeCalculation
{
    GENERATED_BODY()

    virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
};
