// Copyright 2020 Xuelong Mu


#include "MTAbilityTask_PlayMontageAndWaitForEvent.h"

#include "MTAbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/Character.h"


UMTAbilityTask_PlayMontageAndWaitForEvent::UMTAbilityTask_PlayMontageAndWaitForEvent(const FObjectInitializer& ObjectInitializer) :
    Super(ObjectInitializer)
{
}

UMTAbilitySystemComponent* UMTAbilityTask_PlayMontageAndWaitForEvent::GetTargetASC()
{
    return Cast<UMTAbilitySystemComponent>(AbilitySystemComponent);
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::OnMontageBlendingOut(UAnimMontage* Montage, bool bInterrupted)
{
    if (Ability && Ability->GetCurrentMontage() == MontageToPlay)
    {
        if (Montage == MontageToPlay)
        {
            AbilitySystemComponent->ClearAnimatingAbility(Ability);

            // Reset AnimRootMotionTranslationScale
            ACharacter* Character = Cast<ACharacter>(GetAvatarActor());
            if (Character && (Character->GetLocalRole() == ROLE_Authority ||
                (Character->GetLocalRole() == ROLE_AutonomousProxy && Ability->GetNetExecutionPolicy() ==
                    EGameplayAbilityNetExecutionPolicy::LocalPredicted)))
            {
                Character->SetAnimRootMotionTranslationScale(1.f);
            }
        }
    }
    if (bInterrupted)
    {
        if (ShouldBroadcastAbilityTaskDelegates())
        {
            OnInterrupted.Broadcast(FGameplayTag(), FGameplayEventData());
        }
    }
    else
    {
        if (ShouldBroadcastAbilityTaskDelegates())
        {
            OnBlendOut.Broadcast(FGameplayTag(), FGameplayEventData());
        }
    }
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::OnAbilityCancelled()
{
    if (StopPlayingMontage())
    {
        // Let the BP extend on cancel logic as well
        if (ShouldBroadcastAbilityTaskDelegates())
        {
            OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
        }
    }
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::OnMontageEnded(UAnimMontage* Montage, bool bInterrupted)
{
    if (!bInterrupted)
    {
        if (ShouldBroadcastAbilityTaskDelegates())
        {
            OnCompleted.Broadcast(FGameplayTag(), FGameplayEventData());
        }
    }

    EndTask();
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::OnGameplayEvent(FGameplayTag GameplayEventTag,
                                                  const FGameplayEventData* GameplayEventData)
{
    if (ShouldBroadcastAbilityTaskDelegates())
    {
        FGameplayEventData TempData = *GameplayEventData;
        TempData.EventTag = GameplayEventTag;

        GameplayEventReceived.Broadcast(GameplayEventTag, TempData);
    }
}

UMTAbilityTask_PlayMontageAndWaitForEvent* UMTAbilityTask_PlayMontageAndWaitForEvent::PlayMontageAndWaitForEvent(UGameplayAbility* OwningAbility,
    FName TaskInstanceName, UAnimMontage* MontageToPlay, FGameplayTagContainer GameplayEventTags, float Rate,
    FName StartSection, bool bStopWhenAbilityEnds, float AnimRootMotionTranslationScale)
{
    UAbilitySystemGlobals::NonShipping_ApplyGlobalAbilityScaler_Rate(Rate);

    UMTAbilityTask_PlayMontageAndWaitForEvent* MyObj = NewAbilityTask<UMTAbilityTask_PlayMontageAndWaitForEvent>(OwningAbility, TaskInstanceName);
    MyObj->MontageToPlay = MontageToPlay;
    MyObj->GameplayEventTags = GameplayEventTags;
    MyObj->Rate = Rate;
    MyObj->StartSection = StartSection;
    MyObj->AnimRootMotionTranslationScale = AnimRootMotionTranslationScale;
    MyObj->bStopWhenAbilityEnds = bStopWhenAbilityEnds;
    return MyObj;
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::Activate()
{
    if (!Ability) return;

    bool bPlayedMontage = false;
    // UMTAbilitySystemComponent* MTASC = GetTargetASC();

    if (AbilitySystemComponent)
    {
        const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
        UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();
        if (AnimInstance)
        {
            // Bind to gameplay event callback
            GameplayEventHandle = AbilitySystemComponent->AddGameplayEventTagContainerDelegate(
                GameplayEventTags,
                FGameplayEventTagMulticastDelegate::FDelegate::CreateUObject(
                    this, &UMTAbilityTask_PlayMontageAndWaitForEvent::OnGameplayEvent));

            if (AbilitySystemComponent->PlayMontage(Ability, Ability->GetCurrentActivationInfo(), MontageToPlay, Rate,
                                                    StartSection) > 0.f)
            {
                // Playing a montage could potentially fire off a callback into game code which could kill this ability! Early out if we are pending kill.
                if (ShouldBroadcastAbilityTaskDelegates() == false)
                {
                    return;
                }

                CancelledHandle = Ability->OnGameplayAbilityCancelled.AddUObject(
                    this, &UMTAbilityTask_PlayMontageAndWaitForEvent::OnAbilityCancelled);

                BlendingOutDelegate.BindUObject(this, &UMTAbilityTask_PlayMontageAndWaitForEvent::OnMontageBlendingOut);
                AnimInstance->Montage_SetBlendingOutDelegate(BlendingOutDelegate, MontageToPlay);

                MontageEndedDelegate.BindUObject(this, &UMTAbilityTask_PlayMontageAndWaitForEvent::OnMontageEnded);
                AnimInstance->Montage_SetEndDelegate(MontageEndedDelegate, MontageToPlay);

                ACharacter* Character = Cast<ACharacter>(GetAvatarActor());

                // Change scale if this is a character on the server, or on a client-controlled character with local prediction on
                if (Character && (Character->GetLocalRole() == ROLE_Authority || (Character->GetLocalRole() ==
                    ROLE_AutonomousProxy && Ability->GetNetExecutionPolicy() ==
                    EGameplayAbilityNetExecutionPolicy::LocalPredicted)))
                {
                    Character->SetAnimRootMotionTranslationScale(AnimRootMotionTranslationScale);
                }
                bPlayedMontage = true;
            }
        }
        else
        {
            ABILITY_LOG(Warning, TEXT("UPlayMontageAndWaitForEvent call to PlayMontage failed!"));
        }
    }
    else
    {
        ABILITY_LOG(
            Warning, TEXT("UPlayMontageAndWaitForEvent called on invalid AbilitySystemComponent"));
    }

    if (!bPlayedMontage)
    {
        ABILITY_LOG(
            Warning,
            TEXT(
                "UPlayMontageAndWaitForEvent called in Ability %s failed to play montage %s; Task Instance Name %s."
            ),
            *Ability->GetName(), *GetNameSafe(MontageToPlay), *InstanceName.ToString());

        if (ShouldBroadcastAbilityTaskDelegates())
        {
            OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
        }
    }

    SetWaitingOnAvatar();
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::ExternalCancel()
{
    check(AbilitySystemComponent)

    OnAbilityCancelled();

    Super::ExternalCancel();
}

void UMTAbilityTask_PlayMontageAndWaitForEvent::OnDestroy(bool AbilityEnded)
{
    // Note: Clearing montage end delegate isn't necessary since its not a multicast and will be cleared when the next montage plays.
    // (If we are destroyed, it will detect this and not do anything)

    // This delegate, however, should be cleared as it is a multicast
    if (Ability)
    {
        Ability->OnGameplayAbilityCancelled.Remove(CancelledHandle);
        if (AbilityEnded && bStopWhenAbilityEnds)
        {
            StopPlayingMontage();
        }
    }

    if (AbilitySystemComponent)
    {
        AbilitySystemComponent->RemoveGameplayEventTagContainerDelegate(GameplayEventTags, GameplayEventHandle);
    }

    Super::OnDestroy(AbilityEnded);
}

bool UMTAbilityTask_PlayMontageAndWaitForEvent::StopPlayingMontage()
{
    const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
    if (!ActorInfo)
    {
        return false;
    }
    UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();
    if (!AnimInstance)
    {
        return false;
    }


    // Check if the montage is still playing
    // The ability would have been interrupted, in which case we should automatically stop the montage
    if (AbilitySystemComponent && Ability)
    {
        if (AbilitySystemComponent->GetAnimatingAbility() == Ability
            && AbilitySystemComponent->GetCurrentMontage() == MontageToPlay)
        {
            // Unbind delegates so they don't get called as well
            FAnimMontageInstance* MontageInstance = AnimInstance->GetActiveInstanceForMontage(MontageToPlay);
            if (MontageInstance)
            {
                MontageInstance->OnMontageBlendingOutStarted.Unbind();
                MontageInstance->OnMontageEnded.Unbind();
            }
            AbilitySystemComponent->CurrentMontageStop();
            return true;
        }
    }
    return false;
}


FString UMTAbilityTask_PlayMontageAndWaitForEvent::GetDebugString() const
{
    UAnimMontage* PlayingMontage = nullptr;
    if (Ability)
    {
        const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
        UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();

        if (AnimInstance != nullptr)
        {
            PlayingMontage = AnimInstance->Montage_IsActive(MontageToPlay)
                                 ? MontageToPlay
                                 : AnimInstance->GetCurrentActiveMontage();
        }
    }

    return FString::Printf(
        TEXT("PlayMontageAndWaitForEvent. MontageToPlay: %s  (Currently Playing): %s"), *GetNameSafe(MontageToPlay),
        *GetNameSafe(PlayingMontage));
}
