// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AbilitySystemComponent.h"
#include "MTAbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMTAbilitySystemComponent : public UAbilitySystemComponent
{
    GENERATED_BODY()

public:
    bool CharacterAbilitiesGiven = false;
    bool StartupEffectsApplied = false;
};
