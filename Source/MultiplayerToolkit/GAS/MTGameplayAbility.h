// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "MTAbilityTypes.h"
#include "Abilities/GameplayAbility.h"
#include "MultiplayerToolkit/MultiplayerToolkit.h"

#include "MTGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMTGameplayAbility : public UGameplayAbility
{
    GENERATED_BODY()

public:
    UMTGameplayAbility();
    // Abilities with this set will automatically activate when the input is pressed

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
    EMTAbilityInputID AbilityInputID = EMTAbilityInputID::None;

    // Value to associate an ability with an slot without tying it to an automatically activated input.
    // Passive abilities won't be tied to an input so we need a way to generically associate abilities with slots.
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Ability")
    EMTAbilityInputID AbilityID = EMTAbilityInputID::None;

    // Tells an ability to activate immediately when its granted. Used for passive abilities and abilities forced on others.
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Ability")
    bool ActivateAbilityOnGranted = false;

    /** Map of gameplay tags to gameplay effect containers */
    UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category=GameplayEffects)
    TMap<FGameplayTag, FMTGameplayEffectContainer> EffectContainerMap;

    /** Make gameplay effect container spec to be applied later, using the passed in container */
    UFUNCTION(BlueprintCallable, Category = Ability, meta=(AutoCreateRefTerm = "EventData"))
    virtual FMTGameplayEffectContainerSpec MakeEffectContainerSpec(FGameplayTag ContainerTag,
                                                                   const FGameplayEventData& EventData,
                                                                   const FGameplayAbilityTargetDataHandle
                                                                   TargetDataHandle,
                                                                   int32 OverrideGameplayLevel = -1);

    /** Applies a gameplay effect container spec that was previously created */
    UFUNCTION(BlueprintCallable, Category = Ability)
    virtual TArray<FActiveGameplayEffectHandle> ApplyEffectContainerSpec(
        const FMTGameplayEffectContainerSpec& ContainerSpec);
};
