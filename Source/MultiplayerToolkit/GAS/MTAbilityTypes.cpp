#include "MTAbilityTypes.h"
#include "MTAbilitySystemComponent.h"
#include "Chaos/AABB.h"
#include "Chaos/AABB.h"
#include "Chaos/AABB.h"
#include "Chaos/AABB.h"

bool FMTGameplayEffectContainerSpec::HasValidEffects() const
{
    return TargetGameplayEffectSpecs.Num() > 0;
}

bool FMTGameplayEffectContainerSpec::HasValidTargets() const
{
    return TargetData.Num() > 0;
}


// Seems to add HitResults and TargetActors to target array (does not map hit result to target actor)
void FMTGameplayEffectContainerSpec::AddTargets(const TArray<FHitResult>& HitResults,
                                                const TArray<AActor*>& TargetActors)
{
    for (const FHitResult& HitResult : HitResults)
    {
        FGameplayAbilityTargetData_SingleTargetHit* NewData = new FGameplayAbilityTargetData_SingleTargetHit(HitResult);
        TargetData.Add(NewData);
    }

    if (TargetActors.Num() > 0)
    {
        FGameplayAbilityTargetData_ActorArray* NewData = new FGameplayAbilityTargetData_ActorArray();
        NewData->TargetActorArray.Append(TargetActors);
        TargetData.Add(NewData);
    }
}
