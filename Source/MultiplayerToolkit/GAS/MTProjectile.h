// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MTAbilityTypes.h"
#include "GameFramework/Actor.h"
#include "MTProjectile.generated.h"

UCLASS()
class MULTIPLAYERTOOLKIT_API AMTProjectile : public AActor
{
    GENERATED_BODY()

public:
    // Sets default values for this actor's properties
    AMTProjectile();

    /** Spec Container of gameplay effects to apply to targets */
    UPROPERTY(BlueprintReadWrite,Meta=(ExposeOnSpawn=true))
    FMTGameplayEffectContainerSpec EffectContainerSpec;

    /** Damage done to non-GAS actors */
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
    float ExternalDamage = 1.0;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
    class UProjectileMovementComponent* ProjectileMovement;

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;
};
