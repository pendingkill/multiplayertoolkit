#pragma once

// ----------------------------------------------------------------------------------------------------------------
// This header is for Ability-specific structures and enums that are shared across a project
// Every game will probably need a file like this to handle their extensions to the system
// This file is a good place for subclasses of FGameplayEffectContext and FGameplayAbilityTargetData
// ----------------------------------------------------------------------------------------------------------------

#include "GameplayEffectTypes.h"
#include "Abilities/GameplayAbilityTargetTypes.h"

#include "MTAbilityTypes.generated.h"

class UMTAbilitySystemComponent;
class UGameplayEffect;


/**
* Struct defining a list of gameplay effects
* These containers are defined statically in blueprints or assets and then turn into Specs at runtime
*/
USTRUCT(BlueprintType)
struct FMTGameplayEffectContainer
{
    GENERATED_BODY()

public:
    FMTGameplayEffectContainer()
    {
    }

    /** List of gameplay effects to apply to targets */
    UPROPERTY(EditAnywhere,BlueprintReadOnly,Category=GameplayEffectContainer)
    TArray<TSubclassOf<UGameplayEffect>> TargetGameplayEffectClasses;
};

/** A "processed" version of MTGameplayEffectContainer that can be passed around and eventually applied */
USTRUCT(BlueprintType)
struct FMTGameplayEffectContainerSpec
{
    GENERATED_BODY()

public:
    FMTGameplayEffectContainerSpec()
    {
    }

    /** Computed target data */
    UPROPERTY(EditAnywhere,BlueprintReadOnly,Category=GameplayEffectContainer)
    FGameplayAbilityTargetDataHandle TargetData;

    /** List of gameplay effects to apply to targets */
    UPROPERTY(EditAnywhere,BlueprintReadOnly,Category=GameplayEffectContainer)
    TArray<FGameplayEffectSpecHandle> TargetGameplayEffectSpecs;

    /** Returns true if this has any valid effect specs */
    bool HasValidEffects() const;

    /** Returns true if this has any valid targets */
    bool HasValidTargets() const;

    /** Adds new targets to target data */
    void AddTargets(const TArray<FHitResult>& HitResults, const TArray<AActor*>& TargetActors);
};
