// Fill out your copyright notice in the Description page of Project Settings.


#include "MTAttributeSetBase.h"

#include "GameplayEffectExtension.h"
#include "Net/UnrealNetwork.h"

static const float MAX_MANA = 100.0;
static const float MAX_HEALTH = 500.0;
static const float MAX_MOVEMENT_SPEED = 1000.0;

UMTAttributeSetBase::UMTAttributeSetBase()
{
}

void UMTAttributeSetBase::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
    Super::PostGameplayEffectExecute(Data);
    if (Data.EvaluatedData.Attribute == GetManaAttribute())
    {
        SetHealth(FMath::Clamp(GetHealth(), 0.0f, MAX_HEALTH));
    }
    else if (Data.EvaluatedData.Attribute == GetManaAttribute())
    {
        SetMana(FMath::Clamp(GetMana(), 0.0f, MAX_MANA));
    }
    else if (Data.EvaluatedData.Attribute == GetMovementSpeedAttribute())
    {
        SetMovementSpeed(FMath::Clamp(GetMovementSpeed(), 0.0f, MAX_MOVEMENT_SPEED));
    }
}

void UMTAttributeSetBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME_CONDITION_NOTIFY(UMTAttributeSetBase, Health, COND_None, REPNOTIFY_Always);
    DOREPLIFETIME_CONDITION_NOTIFY(UMTAttributeSetBase, Mana, COND_None, REPNOTIFY_Always);
    DOREPLIFETIME_CONDITION_NOTIFY(UMTAttributeSetBase, MovementSpeed, COND_None, REPNOTIFY_Always);
}

void UMTAttributeSetBase::OnRep_Health(const FGameplayAttributeData& OldHealth)
{
    GAMEPLAYATTRIBUTE_REPNOTIFY(UMTAttributeSetBase, Health, OldHealth);
}

void UMTAttributeSetBase::OnRep_Mana(const FGameplayAttributeData& OldMana)
{
    GAMEPLAYATTRIBUTE_REPNOTIFY(UMTAttributeSetBase, Mana, OldMana);
}

void UMTAttributeSetBase::OnRep_MovementSpeed(const FGameplayAttributeData& OldMovementSpeed)
{
    GAMEPLAYATTRIBUTE_REPNOTIFY(UMTAttributeSetBase, MovementSpeed, OldMovementSpeed);
}
