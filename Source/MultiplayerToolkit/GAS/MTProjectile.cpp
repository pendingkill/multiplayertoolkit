// Fill out your copyright notice in the Description page of Project Settings.


#include "MTProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"


// Sets default values
AMTProjectile::AMTProjectile()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

    bReplicates = true;

    ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(FName("ProjectileMovement"));
}

// Called when the game starts or when spawned
void AMTProjectile::BeginPlay()
{
    Super::BeginPlay();
}

// Called every frame
void AMTProjectile::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}
