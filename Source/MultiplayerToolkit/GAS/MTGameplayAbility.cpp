// Fill out your copyright notice in the Description page of Project Settings.


#include "MTGameplayAbility.h"

#include "AbilitySystemComponent.h"

UMTGameplayAbility::UMTGameplayAbility()
{
    // Default to Instance Per Actor
    InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

FMTGameplayEffectContainerSpec UMTGameplayAbility::MakeEffectContainerSpec(FGameplayTag ContainerTag,
                                                                           const FGameplayEventData& EventData,
                                                                           const FGameplayAbilityTargetDataHandle
                                                                           TargetDataHandle,
                                                                           int32 OverrideGameplayLevel)
{
    FMTGameplayEffectContainerSpec EffectContainerSpec;

    // If we don't have an override level, use the default on the ability itself
    if (OverrideGameplayLevel == INDEX_NONE)
    {
        OverrideGameplayLevel = this->GetAbilityLevel();
    }

    FMTGameplayEffectContainer* EffectContainer = EffectContainerMap.Find(ContainerTag);
    if (EffectContainer)
    {
        for (auto GE : EffectContainer->TargetGameplayEffectClasses)
        {
            FGameplayEffectSpecHandle GESpecHandle = MakeOutgoingGameplayEffectSpec(GE, OverrideGameplayLevel);
            EffectContainerSpec.TargetGameplayEffectSpecs.Add(GESpecHandle);
        }
    }

    EffectContainerSpec.TargetData.Append(TargetDataHandle);

    return EffectContainerSpec;
}

TArray<FActiveGameplayEffectHandle> UMTGameplayAbility::ApplyEffectContainerSpec(
    const FMTGameplayEffectContainerSpec& ContainerSpec)
{
    TArray<FActiveGameplayEffectHandle> ActiveGEHandles;

    // Iterate list of effect specs and apply them to their target data
    for (const FGameplayEffectSpecHandle& GESpecHandle : ContainerSpec.TargetGameplayEffectSpecs)
    {
        ActiveGEHandles.Append(K2_ApplyGameplayEffectSpecToTarget(GESpecHandle, ContainerSpec.TargetData));
    }
    return ActiveGEHandles;
}
