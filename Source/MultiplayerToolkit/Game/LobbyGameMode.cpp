// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameMode.h"

#include "MTGameInstance.h"
#include "Kismet/GameplayStatics.h"

const static int MINIMUM_PLAYERS = 2;
const static float GAME_START_COUNTDOWN = 5.f;

ALobbyGameMode::ALobbyGameMode()
{
    bUseSeamlessTravel = true;
}

void ALobbyGameMode::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);
    if (GetNumPlayers() >= MINIMUM_PLAYERS)
    {
        UE_LOG(LogTemp, Warning, TEXT("Num players >= %d, countdown starting"), MINIMUM_PLAYERS);
        GetWorldTimerManager().SetTimer(GameStartTimer, this, &ALobbyGameMode::StartGame,
                                        GAME_START_COUNTDOWN, false);
    }
}

void ALobbyGameMode::Logout(AController* Exiting)
{
    Super::Logout(Exiting);
}

void ALobbyGameMode::StartGame()
{
    auto GI = GetGameInstance();
    if (!ensure(GI))
    {
        return;
    }


    auto MTGI = Cast<UMTGameInstance>(GI);
    if (!ensure(MTGI))
    {
        return;
    }

    MTGI->StartSession();
    GetWorld()->ServerTravel("/Game/Maps/MTStylizedBattleMap?listen");
}
