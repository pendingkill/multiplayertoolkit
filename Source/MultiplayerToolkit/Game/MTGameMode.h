// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"


#include "MultiplayerToolkit.h"
#include "GameFramework/GameMode.h"

#include "MTGameMode.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API AMTGameMode : public AGameMode
{
    GENERATED_BODY()
public:
    AMTGameMode();

    virtual void PostLogin(APlayerController* NewPlayer) override;

    void HeroDied(AController* Controller);
    void RegisterPlayerKilled(AController* Player, AController* Killer);

    UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;

protected:
    UPROPERTY(EditDefaultsOnly)
    float RespawnDelay = 5.0f;

    UPROPERTY(EditDefaultsOnly)
    float MatchTimerSeconds = 5.0f;

    UPROPERTY(EditDefaultsOnly)
    float PostMatchWaitTimerSeconds = 10.0f;

    UPROPERTY(EditDefaultsOnly)
    TMap<EMTPlayerClass, TSubclassOf<class AMTPlayerCharacter>> PlayerClassMap;

    virtual void BeginPlay() override;

    virtual void HandleMatchHasStarted() override;

    virtual void EndMatch() override;

    void RespawnPlayer(AController* Controller);
};
