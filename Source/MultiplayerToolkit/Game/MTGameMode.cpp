// Copyright 2020 Xuelong Mu


#include "MTGameMode.h"


#include "MTGameState.h"
#include "PlayerData.h"
#include "GameFramework/GameStateBase.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "MultiplayerToolkit/Player/MTPlayerController.h"
#include "MultiplayerToolkit/UI/MTHUDWidget.h"
#include "MultiplayerToolkit/Characters/MTPlayerCharacter.h"

AMTGameMode::AMTGameMode()
{
    bUseSeamlessTravel = true;
    DefaultPlayerName = FText::FromString("Player");
}


void AMTGameMode::PostLogin(APlayerController* NewPlayer)
{
    Super::PostLogin(NewPlayer);
    StartMatch(); // Start match when at least one player is in the server
    // Update scoreboard for existing players after new player joins
    TArray<FPlayerInfo> PlayerInfo;
    auto GS = GetGameState<AMTGameState>();
    if (GS)
    {
        GS->GetPlayerScores(PlayerInfo);

        for (auto PlayerState : GS->PlayerArray)
        {
            auto PC = Cast<APlayerController>(PlayerState->GetOwner());
            if (ensure(PC) && PC != NewPlayer)
            {
                auto MTPC = Cast<AMTPlayerController>(PC);
                if (ensure(MTPC))
                {
                    MTPC->RebuildScoreboard(PlayerInfo);
                }
            }
        }
    }
}

// Respawn player
void AMTGameMode::HeroDied(AController* Controller)
{
    Controller->UnPossess();
    FTimerHandle RespawnTimerHandle;
    FTimerDelegate RespawnDelegate = FTimerDelegate::CreateUObject(this, &AMTGameMode::RespawnPlayer, Controller);

    GetWorldTimerManager().SetTimer(RespawnTimerHandle, RespawnDelegate, RespawnDelay, false);
}

// Update score
void AMTGameMode::RegisterPlayerKilled(AController* Player, AController* Killer)
{
    if (Player && Killer)
    {
        UE_LOG(LogTemp, Warning, TEXT("%s was killed"), *Player->GetName());

        if (Killer)
        {
            UE_LOG(LogTemp, Warning, TEXT("%s killed by %s"), *Player->GetName(), *Killer->GetName());
            auto PS = Killer->GetPlayerState<APlayerState>();
            float NewScore = PS->GetScore() + 1.0f;
            PS->SetScore(NewScore);
            PS->OnRep_Score();
        }
    }
}

UClass* AMTGameMode::GetDefaultPawnClassForController_Implementation(AController* InController)
{
    auto PCBase = Cast<APlayerControllerBase>(InController);

    if (ensure(PCBase))
    {
        if (PlayerClassMap.Contains(PCBase->PlayerClass))
        {
            // Maybe a bit dodgy here
            UE_LOG(LogTemp, Warning, TEXT("Found %s as player class"),
                   *UEnum::GetValueAsString(PCBase->PlayerClass));
            return PlayerClassMap.Find(PCBase->PlayerClass)->Get();
        }
        else
        {
            UE_LOG(LogTemp, Error, TEXT("%s player class not mapped in GameMode"),
                   *UEnum::GetValueAsString(PCBase->PlayerClass));
        }
    }
    return DefaultPawnClass;
}


void AMTGameMode::BeginPlay()
{
}


void AMTGameMode::HandleMatchHasStarted()
{
    Super::HandleMatchHasStarted();

    FTimerHandle MatchTimerHandle;
    GetWorldTimerManager().SetTimer(MatchTimerHandle, this, &AMTGameMode::EndMatch, MatchTimerSeconds);
    UE_LOG(LogTemp, Warning, TEXT("Match has started"));
}

void AMTGameMode::EndMatch()
{
    UE_LOG(LogTemp, Warning, TEXT("Match has ended"));
    auto MTGS = GetGameState<AMTGameState>();
    if (MTGS)
    {
        for (APlayerState* PS : MTGS->PlayerArray)
        {
            auto MTPC = Cast<AMTPlayerController>(PS->GetOwner());
            if (MTPC)
            {
                MTPC->HandleMatchHasEnded();
            }
        }
    }
    FTimerHandle PostGameTimerHandle;

    GetWorldTimerManager().SetTimer(PostGameTimerHandle, this, &AMTGameMode::RestartGame, PostMatchWaitTimerSeconds);

    Super::EndMatch();
}


void AMTGameMode::RespawnPlayer(AController* Controller)
{
    if (HasMatchEnded())
    {
        return;
    }
    if (Controller->IsPlayerController())
    {
        auto PlayerStart = FindPlayerStart(Controller);
        FActorSpawnParameters SpawnParameters;
        SpawnParameters.SpawnCollisionHandlingOverride =
            ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
        auto PlayerCharacter = GetWorld()->SpawnActor<AMTPlayerCharacter>(
            GetDefaultPawnClassForController(Controller), PlayerStart->GetActorTransform(), SpawnParameters);
        Controller->Possess(PlayerCharacter);
    }
}
