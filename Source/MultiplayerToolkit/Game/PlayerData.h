﻿#pragma once

#include "PlayerData.generated.h"

USTRUCT(BlueprintType)
struct FPlayerInfo
{
    GENERATED_BODY()

    UPROPERTY()
    int32 PlayerId;

    UPROPERTY()
    FString PlayerName;

    UPROPERTY()
    float Score;
};
