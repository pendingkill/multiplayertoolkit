// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "MultiplayerToolkit/MenuSystem/MenuInterface.h"

#include "MTGameInstance.generated.h"


/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMTGameInstance : public UGameInstance, public IMenuInterface
{
    GENERATED_BODY()

public:
    UMTGameInstance(const FObjectInitializer& ObjectInitializer);

    virtual void Init();

    UFUNCTION(Exec, BlueprintCallable)
    void LoadMenu();

    UFUNCTION(Exec)
    virtual void Host(FString ServerName) override;

    virtual void RefreshServerList() override;

    UFUNCTION(Exec)
    void Join(uint32 Index);

    UFUNCTION(Exec)
    void QuitToMainMenu() override;

    UFUNCTION(Exec)
    void QuitGame() override;

    UFUNCTION()
    void SetPlayerName(const FString& PlayerName) override;

    UFUNCTION(Exec)
    void CreateSession() const;

    UFUNCTION()
    void StartSession() const;

    UPROPERTY()
    FString CustomPlayerName;

private:
    TSubclassOf<UUserWidget> MainMenuClass;

    IOnlineSessionPtr SessionInterface;

    UPROPERTY()
    class UMainMenu* MainMenu;

    TSharedPtr<FOnlineSessionSearch> SessionSearch;

    FString UserSetServerName;

    void OnDestroySessionComplete(FName SessionName, bool Success) const;
    void OnCreateSessionComplete(FName SessionName, bool Success) const;
    void OnFindSessionsComplete(bool Success) const;
    void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result) const;
};
