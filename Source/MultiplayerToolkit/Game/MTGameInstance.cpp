// Fill out your copyright notice in the Description page of Project Settings.

#include "MTGameInstance.h"


#include "UObject/ConstructorHelpers.h"
#include "OnlineSessionSettings.h"

#include "Blueprint/UserWidget.h"
#include "Engine/Engine.h"
#include "MultiplayerToolkit/MenuSystem/MainMenu.h"
#include "OnlineSubsystem.h"
#include "Interfaces/OnlineSessionInterface.h"
#include "Kismet/KismetStringLibrary.h"

const static FName SESSION_NAME = NAME_GameSession;
const static FName SERVER_NAME_SETTINGS_KEY = TEXT("ServerName");

UMTGameInstance::UMTGameInstance(const FObjectInitializer& ObjectInitializer)
{
    UE_LOG(LogTemp, Warning, TEXT("GameInstance Constructor"));

    MainMenuClass = ConstructorHelpers::FClassFinder<UUserWidget>(TEXT("/Game/MenuSystem/WBP_MainMenu")).Class;
}

void UMTGameInstance::Init()
{
    auto Subsystem = IOnlineSubsystem::Get();
    if (Subsystem)
    {
        UE_LOG(LogTemp, Warning, TEXT("Found subsystem: %s"), *Subsystem->GetSubsystemName().ToString());
        SessionInterface = Subsystem->GetSessionInterface();
        if (SessionInterface.IsValid())
        {
            SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(
                this, &UMTGameInstance::OnCreateSessionComplete);
            SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(
                this, &UMTGameInstance::OnDestroySessionComplete);
            SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(
                this, &UMTGameInstance::OnFindSessionsComplete);
            SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(
                this, &UMTGameInstance::OnJoinSessionComplete);

            SessionSearch = MakeShareable(new FOnlineSessionSearch());
        }
    }
    UE_LOG(LogTemp, Warning, TEXT("GameInstance Init"));
}

void UMTGameInstance::LoadMenu()
{
    if (!MainMenuClass) return;
    MainMenu = CreateWidget<UMainMenu>(this, MainMenuClass, "MainMenu");
    MainMenu->Setup();
    MainMenu->SetMenuInterface(this);
}

void UMTGameInstance::Host(FString ServerName)
{
    if (!SessionInterface.IsValid())
    {
        UE_LOG(LogTemp, Error, TEXT("Session Interface not found!"));
        return;
    }
    UserSetServerName = ServerName;
    const auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
    if (ExistingSession)
    {
        SessionInterface->DestroySession(SESSION_NAME);
    }
    else
    {
        CreateSession();
    }
}

void UMTGameInstance::RefreshServerList()
{
    if (SessionSearch.IsValid())
    {
        SessionSearch->bIsLanQuery = false;
        SessionSearch->MaxSearchResults = 100;
        SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
        SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
    }
}


void UMTGameInstance::OnDestroySessionComplete(FName SessionName, bool Success) const
{
    CreateSession();
}

void UMTGameInstance::CreateSession() const
{
    if (!SessionInterface.IsValid())
    {
        return;
    }

    FOnlineSessionSettings SessionSettings;
    if (IOnlineSubsystem::Get()->GetSubsystemName() == NULL_SUBSYSTEM)
    {
        SessionSettings.bIsLANMatch = true;
    }
    else
    {
        SessionSettings.bIsLANMatch = false;
    }
    SessionSettings.bShouldAdvertise = true;
    SessionSettings.NumPublicConnections = 5;
    SessionSettings.bUsesPresence = true;
    SessionSettings.bAllowJoinViaPresence = true;
    SessionSettings.Set(SERVER_NAME_SETTINGS_KEY, UserSetServerName,
                        EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

    SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
}

void UMTGameInstance::StartSession() const
{
    if (!SessionInterface.IsValid())
    {
        return;
    }
    bool Result = SessionInterface->StartSession(SESSION_NAME);
    UE_LOG(LogTemp, Warning, TEXT("Session start successful: %s"),
           *UKismetStringLibrary::Conv_BoolToString(Result));
}

void UMTGameInstance::OnCreateSessionComplete(FName SessionName, bool Success) const
{
    UE_LOG(LogTemp, Warning, TEXT("OnCreateSessionComplete Delegate called"));

    UEngine* Engine = GetEngine();
    if (!ensure(Engine != nullptr)) return;
    if (!Success)
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to create session."));
        return;
    }

    Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));

    UWorld* World = GetWorld();
    if (!ensure(World != nullptr)) return;

    World->ServerTravel("/Game/Maps/Lobby?listen");
}

void UMTGameInstance::OnFindSessionsComplete(bool Success) const
{
    if (Success && SessionSearch.IsValid())
    {
        UE_LOG(LogTemp, Warning, TEXT("OnFindSessionsComplete Delegate called"));


        TArray<FServerInfo> ServerListInfo;
        for (const auto& Result : SessionSearch->SearchResults)
        {
            FServerInfo Info;

            Info.HostUsername = Result.Session.OwningUserName;
            Info.MaxPlayers = Result.Session.SessionSettings.NumPublicConnections;
            Info.CurrentPlayers = Info.MaxPlayers - Result.Session.NumOpenPublicConnections;
            FString ServerName;
            if (Result.Session.SessionSettings.Get(SERVER_NAME_SETTINGS_KEY, ServerName))
            {
                UE_LOG(LogTemp, Warning, TEXT("Session name: %s"), *ServerName);
                Info.ServerName = ServerName;;
            }
            else
            {
                UE_LOG(LogTemp, Warning, TEXT("Didn't find expected data: %s"), *ServerName);
                Info.ServerName = Result.GetSessionIdStr();
            }
            ServerListInfo.Add(Info);
        }
        if (MainMenu)
        {
            MainMenu->UpdateServerList(ServerListInfo);
        }
    }
}

void UMTGameInstance::OnJoinSessionComplete(FName SessionName,
                                            EOnJoinSessionCompleteResult::Type Result) const
{
    if (!ensure(SessionInterface))
    {
        return;
    }

    FString ConnectString;
    if (SessionInterface->GetResolvedConnectString(SessionName, ConnectString))
    {
        APlayerController* PlayerController = GetFirstLocalPlayerController();
        if (!ensure(PlayerController != nullptr)) return;
        /** Leaving the following as sample code for appending parameters on connecting to server
                FString SampleParam = FString("Dog");
                FString NameParam = FString::Printf(TEXT("?Param=%s"), *SampleParam);
                ConnectString.Append(NameParam);
                UE_LOG(LogTemp, Warning, TEXT("ConnectString: %s"), *ConnectString); */

        PlayerController->ClientTravel(ConnectString, ETravelType::TRAVEL_Absolute);
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Could not get connect string"));
    }
}

void UMTGameInstance::Join(uint32 Index)
{
    if (!SessionInterface || !SessionSearch.IsValid())
    {
        return;
    }

    SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Index]);
}

void UMTGameInstance::QuitToMainMenu()
{
    UEngine* Engine = GetEngine();
    if (!ensure(Engine != nullptr)) return;

    Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Quitting to main menu"));

    APlayerController* PlayerController = GetFirstLocalPlayerController();
    if (!ensure(PlayerController != nullptr)) return;
    PlayerController->ClientTravel("/Game/MenuSystem/MainMenu", ETravelType::TRAVEL_Absolute);
}

void UMTGameInstance::QuitGame()
{
    APlayerController* PlayerController = GetFirstLocalPlayerController();
    if (!ensure(PlayerController != nullptr)) return;
    PlayerController->ConsoleCommand("quit");
}

void UMTGameInstance::SetPlayerName(const FString& PlayerName)
{
    CustomPlayerName = PlayerName;
}
