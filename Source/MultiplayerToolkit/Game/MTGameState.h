// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameState.h"

#include "MTGameState.generated.h"

struct FPlayerInfo;

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API AMTGameState : public AGameState
{
    GENERATED_BODY()
public:

    void RequestPlayerScoreboardInfo(class AMTPlayerController* RequestingPlayer);

    void GetPlayerScores(TArray<FPlayerInfo>& PlayerInfoArray);

    virtual void OnRep_ReplicatedHasBegunPlay() override;

    static void PopulatePlayerInfo(APlayerState* MyPlayerState, FPlayerInfo& OutPlayerInfo);
};
