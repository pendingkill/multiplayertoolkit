// Copyright 2020 Xuelong Mu


#include "MTGameState.h"

#include "PlayerData.h"
#include "Algo/StableSort.h"
#include "GameFramework/PlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "MultiplayerToolkit/Player/MTPlayerController.h"
#include "Net/UnrealNetwork.h"


void AMTGameState::RequestPlayerScoreboardInfo(AMTPlayerController* RequestingPlayer)
{
    TArray<FPlayerInfo> PlayerInfo;
    GetPlayerScores(PlayerInfo);
    if (RequestingPlayer)
    {
        UE_LOG(LogTemp, Warning, TEXT("RebuildScoreboard called for %s with Player Array size %d"),
               *RequestingPlayer->GetPlayerState<APlayerState>()->GetPlayerName(), PlayerInfo.Num());
        RequestingPlayer->RebuildScoreboard(PlayerInfo);
    }
}

void AMTGameState::GetPlayerScores(TArray<FPlayerInfo>& PlayerInfoArray)
{
    for (auto MyPlayer : PlayerArray)
    {
        FPlayerInfo Info;
        PopulatePlayerInfo(MyPlayer, Info);
        PlayerInfoArray.Add(Info);
    }
}

void AMTGameState::OnRep_ReplicatedHasBegunPlay()
{
    Super::OnRep_ReplicatedHasBegunPlay();
    UE_LOG(LogTemp, Warning, TEXT("GS has been replicated"));
}

void AMTGameState::PopulatePlayerInfo(APlayerState* MyPlayerState, FPlayerInfo& OutPlayerInfo)
{
    OutPlayerInfo.PlayerId = MyPlayerState->GetPlayerId();
    OutPlayerInfo.PlayerName = MyPlayerState->GetPlayerName();
    OutPlayerInfo.Score = MyPlayerState->GetScore();
}

