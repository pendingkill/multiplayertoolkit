// Copyright 2020 Xuelong Mu

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "Characters/MTPlayerCharacter.h"

#include "ANS_MeleeBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UANS_MeleeBase : public UAnimNotifyState
{
    GENERATED_BODY()

protected:
    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
    TArray<FName> WeaponSocketNames;

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
    TSet<AActor*> TargetActors;

    UPROPERTY(BlueprintReadWrite, EditDefaultsOnly)
    bool bDrawDebugTraces;

    class UStaticMeshComponent* Weapon;

    virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
                             float TotalDuration) override;
    virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation,
                            float FrameDeltaTime) override;
    virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
