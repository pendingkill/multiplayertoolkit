// Copyright 2020 Xuelong Mu


#include "ANS_MeleeBase.h"


#include "DrawDebugHelpers.h"
#include "Abilities/GameplayAbilityTypes.h"
#include "Engine/World.h"

#include "Characters/MTPlayerCharacter.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

void UANS_MeleeBase::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
    const auto Actor = MeshComp->GetOwner();
    if (!Actor->HasAuthority()) return;

    const auto PlayerCharacter = Cast<AMTPlayerCharacter>(Actor);
    if (!PlayerCharacter) return;

    Weapon = PlayerCharacter->Weapon;
    if (!Weapon) return;

    WeaponSocketNames = Weapon->GetAllSocketNames();

    Super::NotifyBegin(MeshComp, Animation, TotalDuration);
}


void UANS_MeleeBase::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime)
{
    if (!Weapon || !Weapon->GetStaticMesh()) return;
    if (!MeshComp->GetOwner()->HasAuthority()) return;

    if (WeaponSocketNames.Num() == 0) return;
    TArray<FHitResult> Hits;

    const auto Start = Weapon->GetSocketTransform(WeaponSocketNames[0], RTS_World).GetLocation();
    const auto End = Weapon->GetSocketTransform(WeaponSocketNames[1]).GetLocation();

    FRotator SweepBoxRot = UKismetMathLibrary::FindLookAtRotation(Start, End);

    float BoxLength = FVector::Dist(Start, End);
    FVector BoxHalfExtent = FVector(BoxLength / 4, BoxLength / 2, BoxLength / 8);
    FCollisionShape CollisionBox = FCollisionShape::MakeBox(BoxHalfExtent);

    TArray<AActor*> ActorsToIgnore;
    ActorsToIgnore.Add(MeshComp->GetOwner());

    UKismetSystemLibrary::BoxTraceMulti(MeshComp, Start, End, BoxHalfExtent, SweepBoxRot,
                                        UEngineTypes::ConvertToTraceType(ECC_Visibility), false, ActorsToIgnore,
                                        bDrawDebugTraces ? EDrawDebugTrace::ForDuration : EDrawDebugTrace::None, Hits,
                                        true, FLinearColor::Green,
                                        FLinearColor::White, 2.0);


    for (const auto Hit : Hits)
    {
        if (Hit.GetActor()) // not sure if necessary
        {
            // TargetActors is a set, so adding unique actors only
            TargetActors.Add(Hit.GetActor());
        }
    }
    Super::NotifyTick(MeshComp, Animation, FrameDeltaTime);
}

void UANS_MeleeBase::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
    if (!MeshComp->GetOwner()->HasAuthority()) return;

    Super::NotifyEnd(MeshComp, Animation);
    // Reset variables, otherwise old data will persist as this class does not get destroyed after NotifyEnd
    Weapon = nullptr;
    WeaponSocketNames.Empty();
    TargetActors.Empty();
}
