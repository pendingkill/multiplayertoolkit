// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuInterface.h"
#include "Blueprint/UserWidget.h"

#include "EscapeMenu.generated.h"

class UButton;
/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UEscapeMenu : public UUserWidget
{
    GENERATED_BODY()
public:
    void SetMenuInterface(IMenuInterface* MyMenuInterface) { this->MenuInterface = MyMenuInterface; }

    UFUNCTION()
    void Setup();

    UFUNCTION()
    void Teardown();

    UFUNCTION()
    void Quit();

protected:
    virtual bool Initialize() override;

    virtual FReply NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent) override;

private:
    UPROPERTY(meta=(BindWidget))
    UButton* QuitButton;

    UPROPERTY(meta=(BindWidget))
    UButton* CancelButton;

    IMenuInterface* MenuInterface;
};
