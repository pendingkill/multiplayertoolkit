// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ServerRow.generated.h"

struct FServerInfo;
/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UServerRow : public UUserWidget
{
    GENERATED_BODY()
public:
    void Setup(class UMainMenu* Parent, FServerInfo ServerInfo, uint32 Index);

    UPROPERTY(BlueprintReadOnly)
    bool Selected = false;

private:
    UPROPERTY(meta=(BindWidget))
    class UTextBlock* ServerNameTextBlock;

    UPROPERTY(meta=(BindWidget))
    class UTextBlock* HostUsernameTextBlock;

    UPROPERTY(meta=(BindWidget))
    class UTextBlock* NumPlayersTextBlock;

    UPROPERTY(meta=(BindWidget))
    class UButton* ServerButton;

    UPROPERTY()
    class UMainMenu* MainMenu;

    UFUNCTION()
    void SetMenuSelectedIndex();

    uint32 Index;
};
