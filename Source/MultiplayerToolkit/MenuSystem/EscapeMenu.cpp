// Fill out your copyright notice in the Description page of Project Settings.


#include "EscapeMenu.h"

#include "Components/Button.h"

bool UEscapeMenu::Initialize()
{
    if (!Super::Initialize()) return false;

    if (!QuitButton || !CancelButton) return false;

    CancelButton->OnClicked.AddDynamic(this, &UEscapeMenu::Teardown);
    QuitButton->OnClicked.AddDynamic(this, &UEscapeMenu::Quit);

    return true;
}

FReply UEscapeMenu::NativeOnKeyDown(const FGeometry& InGeometry, const FKeyEvent& InKeyEvent)
{
    UE_LOG(LogTemp, Warning, TEXT("Key Pressed: %s, %s"), *InKeyEvent.GetKey().ToString(),
           *FKey(EKeys::Escape).ToString());
    if (InKeyEvent.GetKey() == FKey(EKeys::Escape) || InKeyEvent.GetKey() == FKey(EKeys::One))
    {
        UE_LOG(LogTemp, Warning, TEXT("Found Key"));
        FTimerHandle TimerHandle;
        /** For some inexplicable reason, calling Teardown on the same tick as Escape won't trigger the widget to
         *  actually get removed. Any other key, such as the 1 key, WILL work on the same tick.
         *  */
        GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &UEscapeMenu::Teardown, 0.01, false, 0.01);
    }
    return Super::NativeOnKeyDown(InGeometry, InKeyEvent);
}


void UEscapeMenu::Setup()
{
    this->bIsFocusable = true;
    this->AddToViewport();


    auto MenuInputMode = FInputModeUIOnly();
    MenuInputMode.SetWidgetToFocus(this->TakeWidget());
    MenuInputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

    // Potentially use this?
    // APlayerController* PlayerController = GetOwningPlayer();
    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController) return;

    PlayerController->SetInputMode(MenuInputMode);
    PlayerController->bShowMouseCursor = true;
}

void UEscapeMenu::Teardown()
{
    UE_LOG(LogTemp, Warning, TEXT("Teardown called"));
    this->RemoveFromViewport();
    auto GameInputMode = FInputModeGameOnly();

    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController) return;

    PlayerController->SetInputMode(GameInputMode);
    PlayerController->bShowMouseCursor = false;
}

void UEscapeMenu::Quit()
{
    if (!MenuInterface)
    {
        UE_LOG(LogTemp, Error, TEXT("MenuInterface is nullptr in EscapeMenu"))
        return;
    }
    Teardown();

    MenuInterface->QuitToMainMenu();

    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (PlayerController)
    {
        PlayerController->UnPossess();
    }
}
