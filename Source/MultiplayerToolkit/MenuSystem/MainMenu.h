// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "MenuInterface.h"
#include "Blueprint/UserWidget.h"
#include "MainMenu.generated.h"

USTRUCT()
struct FServerInfo
{
    GENERATED_BODY()
    FString ServerName;
    FString HostUsername;
    uint16 CurrentPlayers;
    uint16 MaxPlayers;
};

/**
 * 
 */
UCLASS()
class MULTIPLAYERTOOLKIT_API UMainMenu : public UUserWidget
{
    GENERATED_BODY()
public:
    void SetMenuInterface(IMenuInterface* MyMenuInterface) { this->MenuInterface = MyMenuInterface; }

    void Setup();

    void UpdateServerList(TArray<FServerInfo>& ServerListInfo);

    void SetSelectedIndex(uint32 Index);

    virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

    UPROPERTY(EditDefaultsOnly)
    TSubclassOf<class UServerRow> ServerListEntry;

protected:
    virtual bool Initialize() override;

private:

    UFUNCTION()
    void HostServer();

    UFUNCTION()
    void JoinServer();

    UFUNCTION()
    void OpenHostMenu();

    UFUNCTION()
    void OpenJoinMenu();

    UFUNCTION()
    void RefreshServerList();

    UFUNCTION()
    void OpenMainMenu();

    UFUNCTION()
    void Quit();

    UFUNCTION()
    void PlayerNameChanged(const FText& Text);

    void UpdateChildren();

    UPROPERTY(meta=(BindWidget))
    class UButton* HostMenuButton;

    UPROPERTY(meta=(BindWidget))
    class UButton* JoinMenuButton;

    UPROPERTY(meta=(BindWidget))
    class UEditableTextBox* SessionNameTextBox;

    UPROPERTY(meta=(BindWidget))
    class UEditableTextBox* PlayerNameTextBox;

    UPROPERTY(meta=(BindWidget))
    class UButton* HostButton;

    UPROPERTY(meta=(BindWidget))
    class UButton* JoinButton;

    UPROPERTY(meta=(BindWidget))
    class UButton* RefreshButton;

    UPROPERTY(meta=(BindWidget))
    class UCircularThrobber* RefreshThrobber;

    UPROPERTY(meta=(BindWidget))
    class UButton* CancelHostButton;

    UPROPERTY(meta=(BindWidget))
    class UButton* CancelJoinButton;

    UPROPERTY(meta=(BindWidget))
    class UButton* QuitButton;

    UPROPERTY(meta=(BindWidget))
    class UWidgetSwitcher* MenuSwitcher;

    UPROPERTY(meta=(BindWidget))
    class UWidget* MainMenu;

    UPROPERTY(meta=(BindWidget))
    class UWidget* HostMenu;

    UPROPERTY(meta=(BindWidget))
    class UWidget* JoinMenu;

    UPROPERTY(meta=(BindWidget))
    class UScrollBox* ServerList;

    IMenuInterface* MenuInterface;

    TOptional<uint32> SelectedIndex;
};
