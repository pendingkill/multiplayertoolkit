// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenu.h"

#include "ServerRow.h"
#include "Components/Button.h"
#include "Components/CircularThrobber.h"
#include "Components/EditableTextBox.h"
#include "Components/ScrollBox.h"
#include "Components/WidgetSwitcher.h"

bool UMainMenu::Initialize()
{
    if (!Super::Initialize()) return false;

    if (!HostButton || !JoinButton || !PlayerNameTextBox) return false;

    HostButton->OnClicked.AddDynamic(this, &UMainMenu::HostServer);
    JoinButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);
    HostMenuButton->OnClicked.AddDynamic(this, &UMainMenu::OpenHostMenu);
    JoinMenuButton->OnClicked.AddDynamic(this, &UMainMenu::OpenJoinMenu);
    RefreshButton->OnClicked.AddDynamic(this, &UMainMenu::RefreshServerList);
    CancelJoinButton->OnClicked.AddDynamic(this, &UMainMenu::OpenMainMenu);
    CancelHostButton->OnClicked.AddDynamic(this, &UMainMenu::OpenMainMenu);
    QuitButton->OnClicked.AddDynamic(this, &UMainMenu::Quit);
    PlayerNameTextBox->OnTextChanged.AddDynamic(this, &UMainMenu::PlayerNameChanged);

    return true;
}

void UMainMenu::Setup()
{
    this->bIsFocusable = true;
    this->AddToViewport();


    auto MainMenuInputMode = FInputModeUIOnly();
    MainMenuInputMode.SetWidgetToFocus(this->TakeWidget());
    MainMenuInputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);

    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController) return;

    PlayerController->SetInputMode(MainMenuInputMode);
    PlayerController->bShowMouseCursor = true;
}

void UMainMenu::UpdateServerList(TArray<FServerInfo>& ServerListInfo)
{
    if (!ensure(ServerListEntry))
    {
        return;
    }
    if (RefreshThrobber)
    {
        RefreshThrobber->SetVisibility(ESlateVisibility::Collapsed);
    }

    ServerList->ClearChildren();
    uint32 i = 0;
    for (auto Server : ServerListInfo)
    {
        auto Entry = CreateWidget<UServerRow>(GetWorld(), ServerListEntry);
        Entry->Setup(this, Server, i);
        ServerList->AddChild(Entry);
        ++i;
    }
}

void UMainMenu::SetSelectedIndex(uint32 Index)
{
    SelectedIndex = Index;
    UpdateChildren();
}

void UMainMenu::UpdateChildren()
{
    for (int32 i = 0; i < ServerList->GetChildrenCount(); ++i)
    {
        UServerRow* Row = Cast<UServerRow>(ServerList->GetChildAt(i));
        if (ensure(Row))
        {
            if (SelectedIndex.IsSet() && i == SelectedIndex.GetValue())
            {
                Row->Selected = true;
            }
            else
            {
                Row->Selected = false;
            }
        }
    }
}


void UMainMenu::HostServer()
{
    UE_LOG(LogTemp, Warning, TEXT("Host button clicked"));
    auto ServerName = SessionNameTextBox->GetText().ToString();
    if (!ServerName.IsEmpty() && MenuInterface)
    {
        MenuInterface->Host(ServerName);
    }
}

void UMainMenu::JoinServer()
{
    if (SelectedIndex.IsSet())
    {
        UE_LOG(LogTemp, Warning, TEXT("Selected index %d"), SelectedIndex.GetValue());
        if (MenuInterface)
        {
            MenuInterface->Join(SelectedIndex.GetValue());
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Selected index not set"));
    }
}

void UMainMenu::OpenHostMenu()
{
    if (!ensure(MenuSwitcher) || !ensure(HostMenu)) return;
    MenuSwitcher->SetActiveWidget(HostMenu);
}

void UMainMenu::OpenJoinMenu()
{
    if (!MenuSwitcher || !JoinMenu) return;
    MenuSwitcher->SetActiveWidget(JoinMenu);
    RefreshServerList();
}

void UMainMenu::RefreshServerList()
{
    if (MenuInterface)
    {
        MenuInterface->RefreshServerList();
    }
    if (RefreshThrobber)
    {
        RefreshThrobber->SetVisibility(ESlateVisibility::Visible);
    }
}


void UMainMenu::OpenMainMenu()
{
    if (!MenuSwitcher || !MainMenu) return;
    MenuSwitcher->SetActiveWidget(MainMenu);
}

void UMainMenu::Quit()
{
    if (MenuInterface)
    {
        MenuInterface->QuitGame();
    }
}

void UMainMenu::PlayerNameChanged(const FText& Text)
{
    MenuInterface->SetPlayerName(Text.ToString());
}

void UMainMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
    APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
    if (!PlayerController) return;

    auto GameInputMode = FInputModeGameOnly();
    PlayerController->SetInputMode(GameInputMode);
    PlayerController->bShowMouseCursor = false;

    Super::OnLevelRemovedFromWorld(InLevel, InWorld);
}
