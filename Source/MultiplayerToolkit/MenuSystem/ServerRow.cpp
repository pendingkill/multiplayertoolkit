// Fill out your copyright notice in the Description page of Project Settings.

#include "ServerRow.h"

#include "MainMenu.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UServerRow::Setup(UMainMenu* Parent, FServerInfo ServerInfo, uint32 ServerIndex)
{
    MainMenu = Parent;
    ServerNameTextBlock->SetText(FText::FromString(ServerInfo.ServerName));
    HostUsernameTextBlock->SetText(FText::FromString(ServerInfo.HostUsername));
    NumPlayersTextBlock->SetText(FText::Format(
            NSLOCTEXT("ExampleNamespace", "ExampleServerName", "{0}/{1}"), ServerInfo.CurrentPlayers,
            ServerInfo.MaxPlayers)
    );
    Index = ServerIndex;
    ServerButton->OnClicked.AddDynamic(this, &UServerRow::SetMenuSelectedIndex);
}

// ReSharper disable once CppMemberFunctionMayBeConst (dynamic delegates cannot be const)
void UServerRow::SetMenuSelectedIndex()
{
    MainMenu->SetSelectedIndex(Index);
}
